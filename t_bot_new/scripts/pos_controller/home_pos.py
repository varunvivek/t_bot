#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import Float64


#global declaration of ros node init
rospy.init_node('joint_controller/home_pos', anonymous=True)
rate = rospy.Rate(10) # 10hz

def _publish(topics_list,angle_set):
    for i in range(0,len(topics_list)):
        pub = rospy.Publisher('/t_bot_new/'+ topics_list[i] +'/command', Float64, queue_size=10)
        pub.publish(angle_set[i])
        rate.sleep()


if __name__ == '__main__':
    try:
        topics_list =  ['hip_joint', 'rf_x_joint', 'rf_u_joint', 'rf_l_joint', 'rf_bf_joint', 'rf_ff_joint', 'lf_x_joint', 'lf_u_joint', 'lf_l_joint', 'lf_bf_joint', 'lf_ff_joint', 'rb_x_joint', 'rb_u_joint', 'rb_m_joint', 'rb_l_joint', 'rb_bf_joint', 'rb_ff_joint', 'lb_x_joint', 'lb_u_joint', 'lb_m_joint', 'lb_l_joint', 'lb_bf_joint', 'lb_ff_joint']
        home_pos_angles=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        _publish(topics_list,home_pos_angles)
        rate.sleep()
    except rospy.ROSInterruptException:
        pass

