#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import Float64
import numpy as np

def talker():
    topics_list =  ['hip_joint', 'rf_x_joint', 'rf_u_joint', 'rf_l_joint', 'rf_bf_joint', 'rf_ff_joint', 'lf_x_joint', 'lf_u_joint', 'lf_l_joint', 'lf_bf_joint', 'lf_ff_joint', 'rb_x_joint', 'rb_u_joint', 'rb_m_joint', 'rb_l_joint', 'rb_bf_joint', 'rb_ff_joint', 'lb_x_joint', 'lb_u_joint', 'lb_m_joint', 'lb_l_joint', 'lb_bf_joint', 'lb_ff_joint']
    angles=np.zeros(23)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        count=0
        for topic in topics_list:
            pub = rospy.Publisher('/t_bot_new/'+ topic +'_controller/command', Float64, queue_size=10)
            data= angles[count]
            rospy.loginfo(data)
            pub.publish(data)
            count+=1
        break
    rate.sleep()


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass



# #!/usr/bin/env python
# # license removed for brevity
# import rospy
# from std_msgs.msg import Float64


# #global declaration of ros node init
# rospy.init_node('joint_controller_home_pos', anonymous=True)
# rate = rospy.Rate(10) # 10hz

# def publish_func(topics_list):
#     for i in range(0,len(topics_list)):
#         pub = rospy.Publisher('/t_bot_new/'+ topics_list[i] +'_controller/command', Float64, queue_size=10)
#         pub.publish(data=0.0)
#         rate.sleep()
#         print(topics_list[i] )


# if __name__ == '__main__':
#     try:
#         topics_list =  ['hip_joint', 'rf_x_joint', 'rf_u_joint', 'rf_l_joint', 'rf_bf_joint', 'rf_ff_joint', 'lf_x_joint', 'lf_u_joint', 'lf_l_joint', 'lf_bf_joint', 'lf_ff_joint', 'rb_x_joint', 'rb_u_joint', 'rb_m_joint', 'rb_l_joint', 'rb_bf_joint', 'rb_ff_joint', 'lb_x_joint', 'lb_u_joint', 'lb_m_joint', 'lb_l_joint', 'lb_bf_joint', 'lb_ff_joint']
#         publish_func(topics_list)
#         rate.sleep()
#     except rospy.ROSInterruptException:
#         pass
