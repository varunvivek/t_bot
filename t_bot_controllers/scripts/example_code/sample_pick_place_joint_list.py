#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import Float64
import numpy as np

#global declaration of ros node init
rospy.init_node('joint_controller', anonymous=True)
rate = rospy.Rate(10) # 10hz

def pick_place(angle_set1,angle_set2):
    itter = 1
    pub1 = rospy.Publisher('/rrbot/joint'+ str(itter) +'_position_controller/command', Float64, queue_size=10)
    pub2 = rospy.Publisher('/rrbot/joint'+ str(itter+1) +'_position_controller/command', Float64, queue_size=10)
    
    #pick
    angles_set1 = np.linspace(angle_set1[0],angle_set1[1],100)
    angles_set2 = np.linspace(angle_set2[0],angle_set2[1],100)
    publisher_loop(pub1, pub2, angles_set1, angles_set2)
    
    #place
    angles_set1 = np.linspace(angle_set1[0],angle_set1[2],100)
    angles_set2 = np.linspace(angle_set2[0],angle_set2[2],100)
    publisher_loop(pub1, pub2, angles_set1, angles_set2)

def publisher_loop(pub1, pub2, angles_set1, angles_set2):
    for i in range(0,100):
        publisherFun(pub1, pub2, angles_set1[i], angles_set2[i])
    for j in range(99,-1,-1):
        publisherFun(pub1, pub2, angles_set1[j], angles_set2[j])

def publisherFun(pub1, pub2, val1, val2):
    pub1.publish(val1)
    pub2.publish(val2)
    rate.sleep()


if __name__ == '__main__':
    try:
        joint_angle_range = np.arange(0.0, 1.5708, 0.1)
        topics_list = ['/joint1','/joint2','/joint3']
        pick_place([0.0,1.3,-1.3],[0.0,1.5,-1.5])
        rate.sleep()
    except rospy.ROSInterruptException:
        pass

