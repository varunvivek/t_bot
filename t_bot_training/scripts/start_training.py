#!/usr/bin/env python

'''
    Original Training code made by Ricardo Tellez <rtellez@theconstructsim.com>
    Moded by Miguel Angel Rodriguez <duckfrost@theconstructsim.com>
    Visit our website at www.theconstructsim.com
'''
import os
import time
import random
import numpy as np
import matplotlib.pyplot as plt
# import pybullet_envs
import gym
import torch
import torch.nn as nn
import torch.nn.functional as F
from gym import wrappers
from torch.autograd import Variable
from collections import deque

# import numpy
import random
# import qlearn #custom py file
from std_msgs.msg import Float64
# ROS packages required
import rospy
import rospkg

# import our training environment
import tbot_env #custom py file


class ReplayBuffer(object):

  def __init__(self, max_size=1e6):
    self.storage = []
    self.max_size = max_size
    self.ptr = 0

  def add(self, transition):
    #print('replay_buffer_trans' + str(transition))
    if len(self.storage) == self.max_size:
      self.storage[int(self.ptr)] = transition
      self.ptr = (self.ptr + 1) % self.max_size
    else:
      self.storage.append(transition)

  def sample(self, batch_size):
    #print('sample' + str(batch_size))
    #print("storage: "+ str(self.storage))
    ind = np.random.randint(0, len(self.storage), size=batch_size)
    #print('sample_ind: '+ str(ind)+ " "+str(len(ind)))
    batch_states, batch_next_states, batch_actions, batch_rewards, batch_dones = [], [], [], [], []
    for i in ind: 
      state, next_state, action, reward, done = self.storage[i]
      batch_states.append(np.array(state, copy=False))
      batch_next_states.append(np.array(next_state, copy=False))
      #print('sample_action: ' +str(action)+' '+ str(i))
      batch_actions.append(np.array(action, copy=False))
      batch_rewards.append(np.array(reward, copy=False))
      batch_dones.append(np.array(done, copy=False))
    return np.array(batch_states), np.array(batch_next_states), np.array(batch_actions), np.array(batch_rewards).reshape(-1, 1), np.array(batch_dones).reshape(-1, 1)

class Actor(nn.Module):
  
  def __init__(self, state_dim, action_dim, max_action):
    super(Actor, self).__init__()
    self.layer_1 = nn.Linear(state_dim, 400)
    self.layer_2 = nn.Linear(400, 300)
    self.layer_3 = nn.Linear(300, action_dim)
    self.max_action = max_action

  def forward(self, x):
    x = F.relu(self.layer_1(x))
    x = F.relu(self.layer_2(x))
    x = self.max_action * torch.tanh(self.layer_3(x))
    return x

class Critic(nn.Module):
  
  def __init__(self, state_dim, action_dim):
    super(Critic, self).__init__()
    # Defining the first Critic neural network
    self.layer_1 = nn.Linear(state_dim + action_dim, 400)
    self.layer_2 = nn.Linear(400, 300)
    self.layer_3 = nn.Linear(300, 1)
    # Defining the second Critic neural network
    self.layer_4 = nn.Linear(state_dim + action_dim, 400)
    self.layer_5 = nn.Linear(400, 300)
    self.layer_6 = nn.Linear(300, 1)

  def forward(self, x, u):
    xu = torch.cat([x, u], 1)
    # Forward-Propagation on the first Critic Neural Network
    x1 = F.relu(self.layer_1(xu))
    x1 = F.relu(self.layer_2(x1))
    x1 = self.layer_3(x1)
    # Forward-Propagation on the second Critic Neural Network
    x2 = F.relu(self.layer_4(xu))
    x2 = F.relu(self.layer_5(x2))
    x2 = self.layer_6(x2)
    return x1, x2

  def Q1(self, x, u):
    xu = torch.cat([x, u], 1)
    x1 = F.relu(self.layer_1(xu))
    x1 = F.relu(self.layer_2(x1))
    x1 = self.layer_3(x1)
    return x1

# Selecting the device (CPU or GPU)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
#print(device)
# Building the whole Training Process into a class

class TD3(object):
  
  def __init__(self, state_dim, action_dim, max_action):
    self.actor = Actor(state_dim, action_dim, max_action).to(device)
    self.actor_target = Actor(state_dim, action_dim, max_action).to(device)
    self.actor_target.load_state_dict(self.actor.state_dict())
    self.actor_optimizer = torch.optim.Adam(self.actor.parameters())
    self.critic = Critic(state_dim, action_dim).to(device)
    self.critic_target = Critic(state_dim, action_dim).to(device)
    self.critic_target.load_state_dict(self.critic.state_dict())
    self.critic_optimizer = torch.optim.Adam(self.critic.parameters())
    self.max_action = max_action

  def select_action(self, state):
    #print("state")
    #print(state)
    state = torch.Tensor(state.reshape(1, -1)).to(device)
    action = self.actor(state).cpu().data.numpy().flatten()
    #print("action")
    #print(action)
    return self.actor(state).cpu().data.numpy().flatten()

  def train(self, replay_buffer, iterations, batch_size=100, discount=0.99, taunoise_clip=0.005, policy_noise=0.2, noise_clip=0.5, policy_freq=2):
    #print("train++++++++++++++++++")
    for it in range(iterations):

      batch_states, batch_next_states, batch_actions, batch_rewards, batch_dones = replay_buffer.sample(batch_size)
      state = torch.Tensor(batch_states).to(device)
      next_state = torch.Tensor(batch_next_states).to(device)
      #print('batch_actions: ' + str(batch_actions) + '\nshape :'+str(batch_actions.shape))
      if type(batch_actions)==np.ndarray:
        batch_action=[]
        for a in batch_actions:
          aa=a.tolist()
          if len(aa)<action_dim:
            aa=aa[0]
          batch_action.append(aa)
          #print('batch_action_+++++++++++: ')
          #print('end for loop')
        batch_actions = batch_action

        
      #print('batch_actions type: ' + str(type(batch_actions)))
      #print('iterations: '+str(it))
      # if type(batch_actions)
      action = torch.Tensor(batch_actions).to(device)
      reward = torch.Tensor(batch_rewards).to(device)
      done = torch.Tensor(batch_dones).to(device)

      next_action = self.actor_target(next_state)
      #print("next_action: " + str(next_action))
      #print("batch_actions_size: " + str(batch_actions.size()))
      #print("batch_actions: " + str(batch_actions))
      noise = torch.Tensor(batch_actions).data.normal_(0, policy_noise).to(device)
      #print("noise_befpre_clip: " + str((batch_actions)))
      noise = noise.clamp(-noise_clip, noise_clip)
      #print("noise: " + str(noise.size()))
      next_action = (next_action + noise).clamp(-self.max_action, self.max_action)
      
      target_Q1, target_Q2 = self.critic_target(next_state, next_action)
      
      target_Q = torch.min(target_Q1, target_Q2)
      
      target_Q = reward + ((1 - done) * discount * target_Q).detach()
      
      current_Q1, current_Q2 = self.critic(state, action)
      
      critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(current_Q2, target_Q)

      self.critic_optimizer.zero_grad()
      critic_loss.backward()
      self.critic_optimizer.step()
      
      if it % policy_freq == 0:
        actor_loss = -self.critic.Q1(state, self.actor(state)).mean()
        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()
        
        for param, target_param in zip(self.actor.parameters(), self.actor_target.parameters()):
          target_param.data.copy_(tau * param.data + (1 - tau) * target_param.data)
        
        for param, target_param in zip(self.critic.parameters(), self.critic_target.parameters()):
          target_param.data.copy_(tau * param.data + (1 - tau) * target_param.data)
  
  # Making a save method to save a trained model
  def save(self, filename, directory):
    torch.save(self.actor.state_dict(), '%s/%s_actor.pth' % (directory, filename))
    torch.save(self.critic.state_dict(), '%s/%s_critic.pth' % (directory, filename))
  
  # Making a load method to load a pre-trained model
  def load(self, filename, directory):
    self.actor.load_state_dict(torch.load('%s/%s_actor.pth' % (directory, filename)))
    self.critic.load_state_dict(torch.load('%s/%s_critic.pth' % (directory, filename)))


def evaluate_policy(policy, eval_episodes=10):
  avg_reward = 0.
  for _ in range(eval_episodes):
    #print("before env reset")
    obs = env.reset()
    done = False
    while not done:
      #print("before select_action")
      #print(type(obs))
      action = policy.select_action(np.array(obs))
      
      #print("before env.step(action)")
      obs, reward, done, _ = env.step(action)
      avg_reward += reward
  avg_reward /= eval_episodes
  print ("---------------------------------------")
  print ("Average Reward over the Evaluation Step: %f" % (avg_reward))
  print ("---------------------------------------")
  return avg_reward


if __name__ == '__main__':
    
    rospy.init_node('tbot_gym', anonymous=True, log_level=rospy.INFO)

    # Create the Gym environment
    env_name = 'tbot-v0'
    env = gym.make(env_name)
    rospy.logdebug ( "Gym environment done")
    reward_pub = rospy.Publisher('/t_bot/reward', Float64, queue_size=1)
    episode_reward_pub = rospy.Publisher('/t_bot/episode_reward', Float64, queue_size=1)

    # Set the logging system
    rospack = rospkg.RosPack()
    pkg_path = rospack.get_path('t_bot_training')
    outdir = pkg_path + '/training_results'
    env = wrappers.Monitor(env, outdir, force=True)
    rospy.logdebug("Monitor Wrapper started")
    
    last_time_steps = np.ndarray(0)
    """
        # Loads parameters from the ROS param server
        # Parameters are stored in a yaml file inside the config directory
        # They are loaded at runtime by the launch file
        Alpha = rospy.get_param("/alpha")
        Epsilon = rospy.get_param("/epsilon")
        Gamma = rospy.get_param("/gamma")
        epsilon_discount = rospy.get_param("/epsilon_discount")
        nepisodes = rospy.get_param("/nepisodes")
        nsteps = rospy.get_param("/nsteps")
    """
    """
    hc parameters
    """

    seed = 0 # Random seed number
    start_timesteps = 1e4 # Number of iterations/timesteps before which the model randomly chooses an action, and after which it starts to use the policy network
    eval_freq = 5e3 # How often the evaluation step is performed (after how many timesteps)
    max_timesteps = 5e4 # Total number of iterations/timesteps
    save_models = True # Boolean checker whether or not to save the pre-trained model
    expl_noise = 0.1 # Exploration noise - STD value of exploration Gaussian noise
    batch_size = 100 # Size of the batch
    discount = 0.99 # Discount factor gamma, used in the calculation of the total discounted reward
    tau = 0.005 # Target network update rate
    policy_noise = 0.2 # STD of Gaussian noise added to the actions for the exploration purposes
    noise_clip = 0.5 # Maximum value of the Gaussian noise added to the actions (policy)
    policy_freq = 2 # Number of iterations to wait before the policy network (Actor model) is updated
    """
    end of hc parameters
    """

    file_name = "%s_%s_%s" % ("TD3", env_name, str(seed))
    print ("---------------------------------------")
    print ("Settings: %s" % (file_name))
    print ("---------------------------------------")

    if not os.path.exists("./results"):
        os.makedirs("./results")
    if save_models and not os.path.exists("./pytorch_models"):
        os.makedirs("./pytorch_models")

    env = gym.make(env_name) 

    env.seed(seed)
    torch.manual_seed(seed)
    np.random.seed(seed)
    state_dim = 23
    action_dim = 18
    max_action = 1.0
    #print("before policy ")
    policy = TD3(state_dim, action_dim, max_action)
    #print("before replay_buffer ")
    
    #print("before evaluations ")
    evaluations = [evaluate_policy(policy)]
    replay_buffer = ReplayBuffer()
    #print("before make dit ")
    def mkdir(base, name):
        path = os.path.join(base, name)
        if not os.path.exists(path):
            os.makedirs(path)
        return path
    work_dir = mkdir('exp', 'brs')
    monitor_dir = mkdir(work_dir, 'monitor')
    max_episode_steps = env._max_episode_steps
    save_env_vid = False
    if save_env_vid:
        env = wrappers.Monitor(env, monitor_dir, force = True)
        env.reset()

    total_timesteps = 0
    timesteps_since_eval = 0
    episode_num = 0
    done = True
    t0 = time.time()
    # We start the main loop over 500,000 timesteps
    while total_timesteps < max_timesteps:
        #print("in while loop")
        # If the episode is done
        if done:
            #print("done: "+str(total_timesteps))

            # If we are not at the very beginning, we start the training process of the model
            if total_timesteps != 0:
                #print("Total Timesteps: {} Episode Num: {} Reward: {}".format(total_timesteps, episode_num, episode_reward))
                policy.train(replay_buffer, episode_timesteps, batch_size, discount, tau, policy_noise, noise_clip, policy_freq)

            # We evaluate the episode and we save the policy
            if timesteps_since_eval >= eval_freq:
                timesteps_since_eval %= eval_freq
                evaluations.append(evaluate_policy(policy))
                policy.save(file_name, directory="./pytorch_models")
                np.save("./results/%s" % (file_name), evaluations)
            
            # When the training step is done, we reset the state of the environment
            obs = env.reset()
            
            # Set the Done to False
            done = False
            #print("done: False")
            # Set rewards and episode timesteps to zero
            episode_reward = 0
            episode_timesteps = 0
            episode_num += 1
        
        # Before 10000 timesteps, we play random actions
        if total_timesteps < start_timesteps:
            
            action = env.action_space.sample()
            #print('action: '+str(action))
        else: # After 10000 timesteps, we switch to the model
            action = policy.select_action(np.array(obs))
            
            # If the explore_noise parameter is not 0, we add noise to the action and we clip it
            if expl_noise != 0:
                #print("++++++++++++++++++++++++++++++++++  ")
                #print("exploit  ")
                rand =np.random.normal(0, expl_noise, size=env.action_space.shape[0])
                #print("rand 341: "+ str(len(rand)))
                #print('action_eploit: '+ str(action))
                action = (action + rand).clip(env.action_space.low, env.action_space.high)
        
        # The agent performs the action in the environment, then reaches the next state and receives the reward
        print("Total Timesteps: {} Episode Num: {} Reward: {}".format(total_timesteps, episode_num, episode_reward))
        #print("++++++++++++++++++++++++++++++++++  ")
        # print(action)
        new_obs, reward, done, _ = env.step(action)
        print(done)
        # We check if the episode is done
        done_bool = 0 if episode_timesteps + 1 == env._max_episode_steps else float(done)
        
        # We increase the total reward
        episode_reward += reward
        
        # We store the new transition into the Experience Replay memory (ReplayBuffer)
        replay_buffer.add((obs, new_obs, action, reward, done_bool))

        # We update the state, the episode timestep, the total timesteps, and the timesteps since the evaluation of the policy
        obs = new_obs
        episode_timesteps += 1
        total_timesteps += 1
        timesteps_since_eval += 1

    # We add the last policy evaluation to our list of evaluations and we save our model
    evaluations.append(evaluate_policy(policy))
    if save_models: policy.save("%s" % (file_name), directory="./pytorch_models")
    np.save("./results/%s" % (file_name), evaluations)

