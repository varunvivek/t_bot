#!/usr/bin/env python

import rospy
import math
from std_msgs.msg import String
from std_msgs.msg import Float64

class JointPub(object):
    def __init__(self):

        self.publishers_array = []
        self._joint_pub=[]
        joint_names_array=['hip_joint','lb_bf','lb_ff','lb_l','lb_m','lb_u','lb_x',
                                        'lf_bf','lf_ff','lf_l','lf_u','lf_x',
                                        'rb_bf','rb_ff','rb_l','rb_m','rb_u','rb_x',
                                        'rf_bf','rf_ff','rf_l','rf_u','rf_x']

                                        
        self._hip_joint_pub = rospy.Publisher('/t_bot_new/hip_joint_controller/command', Float64, queue_size=1)
        self._lb_bf_joint_pub = rospy.Publisher('/t_bot_new/lb_bf_joint_controller/command', Float64, queue_size=1)
        self._lb_ff_joint_pub = rospy.Publisher('/t_bot_new/lb_ff_joint_controller/command', Float64, queue_size=1)
        self._lb_l_joint_pub = rospy.Publisher('/t_bot_new/lb_l_joint_controller/command', Float64, queue_size=1)
        self._lb_m_joint_pub = rospy.Publisher('/t_bot_new/lb_m_joint_controller/command', Float64, queue_size=1)
        self._lb_u_joint_pub = rospy.Publisher('/t_bot_new/lb_u_joint_controller/command', Float64, queue_size=1)
        self._lb_x_joint_pub = rospy.Publisher('/t_bot_new/lb_x_joint_controller/command', Float64, queue_size=1)
        self._lf_bf_joint_pub = rospy.Publisher('/t_bot_new/lf_bf_joint_controller/command', Float64, queue_size=1)
        self._lf_ff_joint_pub = rospy.Publisher('/t_bot_new/lf_ff_joint_controller/command', Float64, queue_size=1)
        self._lf_l_joint_pub = rospy.Publisher('/t_bot_new/lf_l_joint_controller/command', Float64, queue_size=1)
        self._lf_u_joint_pub = rospy.Publisher('/t_bot_new/lf_u_joint_controller/command', Float64, queue_size=1)
        self._lf_x_joint_pub = rospy.Publisher('/t_bot_new/lf_x_joint_controller/command', Float64, queue_size=1)
        self._rb_bf_joint_pub = rospy.Publisher('/t_bot_new/rb_bf_joint_controller/command', Float64, queue_size=1)
        self._rb_ff_joint_pub = rospy.Publisher('/t_bot_new/rb_ff_joint_controller/command', Float64, queue_size=1)
        self._rb_l_joint_pub = rospy.Publisher('/t_bot_new/rb_l_joint_controller/command', Float64, queue_size=1)
        self._rb_m_joint_pub = rospy.Publisher('/t_bot_new/rb_m_joint_controller/command', Float64, queue_size=1)
        self._rb_u_joint_pub = rospy.Publisher('/t_bot_new/rb_u_joint_controller/command', Float64, queue_size=1)
        self._rb_x_joint_pub = rospy.Publisher('/t_bot_new/rb_x_joint_controller/command', Float64, queue_size=1)
        self._rf_bf_joint_pub = rospy.Publisher('/t_bot_new/rf_bf_joint_controller/command', Float64, queue_size=1)
        self._rf_bf_joint_pub = rospy.Publisher('/t_bot_new/rf_ff_joint_controller/command', Float64, queue_size=1)
        self._rf_l_joint_pub = rospy.Publisher('/t_bot_new/rf_l_joint_controller/command', Float64, queue_size=1)
        self._rf_u_joint_pub = rospy.Publisher('/t_bot_new/rf_u_joint_controller/command', Float64, queue_size=1)
        self._rf_x_joint_pub = rospy.Publisher('/t_bot_new/rf_x_joint_controller/command', Float64, queue_size=1)

        
        self.publishers_array.append(self._hip_joint_pub)
        self.publishers_array.append(self._lb_bf_joint_pub)
        self.publishers_array.append(self._lb_ff_joint_pub)
        self.publishers_array.append(self._lb_l_joint_pub)
        self.publishers_array.append(self._lb_m_joint_pub)
        self.publishers_array.append(self._lb_u_joint_pub)
        self.publishers_array.append(self._lb_x_joint_pub)
        self.publishers_array.append(self._lf_bf_joint_pub)
        self.publishers_array.append(self._lf_ff_joint_pub)
        self.publishers_array.append(self._lf_l_joint_pub)
        self.publishers_array.append(self._lf_u_joint_pub)
        self.publishers_array.append(self._lf_x_joint_pub)
        self.publishers_array.append(self._rb_bf_joint_pub)
        self.publishers_array.append(self._rb_ff_joint_pub)
        self.publishers_array.append(self._rb_l_joint_pub)
        self.publishers_array.append(self._rb_m_joint_pub)
        self.publishers_array.append(self._rb_u_joint_pub)
        self.publishers_array.append(self._rb_x_joint_pub)
        self.publishers_array.append(self._rf_bf_joint_pub)
        self.publishers_array.append(self._rf_ff_joint_pub)
        self.publishers_array.append(self._rf_l_joint_pub)
        self.publishers_array.append(self._rf_u_joint_pub)
        self.publishers_array.append(self._rf_x_joint_pub)
        self.init_pos = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]

    def set_init_pose(self):
        """
        Sets joints to initial position [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        :return:
        """
        self.check_publishers_connection()
        self.move_joints(self.init_pos)


    def check_publishers_connection(self):
        """
        Checks that all the publishers are working
        :return:
        """
        rate = rospy.Rate(10)  # 10hz
        while (self._haa_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _haa_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_haa_joint_pub Publisher Connected")
max_height=self.max_height,
                                                    min_height=self.min_height,
                                                    abs_max_roll=self.max_incl,
                                                    abs_max_pitch=self.max_incl,
                                                    joint_increment_value=self.joint_increment_value,
                                                    done_reward=self.done_reward,
                                                    alive_reward=self.alive_reward,
                                                    desired_force=self.desired_force,
                                                    desired_yaw=self.desired_yaw,
                                                    weight_r1=self.weight_r1,
                                                    weight_r2=self.weight_r2,
                                                    weight_r3=self.weight_r3,
                                                    weight_r4=self.weight_r4,
                                                    weight_r5=self.weight_r5
        while (self._hfe_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _hfe_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_hfe_joint_pub Publisher Connected")

        while (self._kfe_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _kfe_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_kfe_joint_pub Publisher Connected")

        rospy.logdebug("All Publishers READY")

    def joint_mono_des_callback(self, msg):
        rospy.logdebug(str(msg.joint_state.position))

        self.move_joints(msg.joint_state.position)

    def move_joints(self, joints_array):

        i = 0
        for publisher_object in self.publishers_array:
          joint_value = Float64()
          joint_value.data = joints_array[i]
          rospy.logdebug("JointsPos>>"+str(joint_value))
          publisher_object.publish(joint_value)
          i += 1


    def start_loop(self, rate_value = 2.0):
        rospy.logdebug("Start Loop")
        pos1 = [0.0,0.0,1.6]
        pos2 = [0.0,0.0,-1.6]
        position = "pos1"
        rate = rospy.Rate(rate_value)
        while not rospy.is_shutdown():
          if position == "pos1":
            self.move_joints(pos1)
            position = "pos2"
          else:
            self.move_joints(pos2)
            position = "pos1"
          rate.sleep()

    def start_sinus_loop(self, rate_value = 2.0):
        rospy.logdebug("Start Loop")
        w = 0.0
        x = 2.0*math.sin(w)
        #pos_x = [0.0,0.0,x]
        #pos_x = [x, 0.0, 0.0]
        pos_x = [0.0, x, 0.0]
        rate = rospy.Rate(rate_value)
        while not rospy.is_shutdown():
            self.move_joints(pos_x)
            w += 0.05
            x = 2.0 * math.sin(w)
            #pos_x = [0.0, 0.0, x]
            #pos_x = [x, 0.0, 0.0]
            pos_x = [0.0, x, 0.0]
            rate.sleep()


if __name__=="__main__":
    rospy.init_node('joint_publisher_node')
    joint_publisher = JointPub()
    rate_value = 50.0
    #joint_publisher.start_loop(rate_value)
    joint_publisher.start_sinus_loop(rate_value)
