#!/usr/bin/env python

import rospy
from gazebo_msgs.msg import ContactsState
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Quaternion, Vector3
from sensor_msgs.msg import JointState
from std_msgs.msg import Float64
import tf
import numpy
import math


class TbotState(object):

    def __init__(self, max_height, min_height, abs_max_roll, abs_max_pitch, joint_increment_value = 0.05, done_reward = -1000.0, alive_reward=10.0, desired_force=7.08, desired_yaw=0.0, weight_r1=1.0, weight_r2=1.0, weight_r3=1.0, weight_r4=1.0, weight_r5=1.0, discrete_division=10):
        rospy.logdebug("Starting TbotState Class object...")
        self.desired_world_point = Vector3(0.0, 0.0, 0.0)
        self._min_height = min_height
        self._max_height = max_height
        self._abs_max_roll = abs_max_roll
        self._abs_max_pitch = abs_max_pitch
        self._joint_increment_value = joint_increment_value
        self._done_reward = done_reward
        self._alive_reward = alive_reward
        self._desired_force = desired_force
        self._desired_yaw = desired_yaw

        self._weight_r1 = weight_r1
        self._weight_r2 = weight_r2
        self._weight_r3 = weight_r3
        self._weight_r4 = weight_r4
        self._weight_r5 = weight_r5
        #  "joint_states_haa",
        #  "joint_states_hfe",
        #  "joint_states_kfe",
        # self._list_of_observations = ["distance_from_desired_point",
        #          "base_roll",
        #          "base_pitch",
        #          "base_yaw",
        #          "contact_force",
        #          "joint_states_hip","joint_states_lb_bf","joint_states_lb_ff","joint_states_lb_l","joint_states_lb_m","joint_states_lb_u","joint_states_lb_x",
        #          "joint_states_lf_bf","joint_states_lf_ff","joint_states_lf_l","joint_states_lf_u","joint_states_lf_x",
        #          "joint_states_rb_bf","joint_states_rb_ff","joint_states_rb_l","joint_states_rb_m","joint_states_rb_u","joint_states_rb_x",
        #          "joint_states_rf_bf","joint_states_rf_ff","joint_states_rf_l","joint_states_rf_u","joint_states_rf_x"]

         self._list_of_observations = ["distance_from_desired_point",
                 "base_roll",
                 "base_pitch",
                 "base_yaw",
                 "contact_force",
                 "joint_states_lb_bf","joint_states_lb_ff","joint_states_lb_l","joint_states_lb_m","joint_states_lb_u",
                 "joint_states_lf_bf","joint_states_lf_ff","joint_states_lf_l","joint_states_lf_u",
                 "joint_states_rb_bf","joint_states_rb_ff","joint_states_rb_l","joint_states_rb_m","joint_states_rb_u",
                 "joint_states_rf_bf","joint_states_rf_ff","joint_states_rf_l","joint_states_rf_u"]

       
        self._discrete_division = discrete_division
        # We init the observation ranges and We create the bins now for all the observations
        self.init_bins()

        self.base_position = Point()
        self.base_orientation = Quaternion()
        self.base_linear_acceleration = Vector3()
        self.contact_force_lb_bf = Vector3()
        self.contact_force_lb_ff = Vector3()
        self.contact_force_lf_bf = Vector3()
        self.contact_force_lf_ff = Vector3()
        self.contact_force_rb_bf = Vector3()
        self.contact_force_rb_ff = Vector3()
        self.contact_force_rf_bf = Vector3()
        self.contact_force_rf_ff = Vector3()
        self.joints_state = JointState()
        
        # self.process_val_hip = 0.0
        self.process_val_lb_bf = 0.0
        self.process_val_lb_ff = 0.0
        self.process_val_lb_l = 0.0
        self.process_val_lb_m = 0.0
        self.process_val_lb_u = 0.0
        # self.process_val_lb_x = 0.0
        self.process_val_lf_bf = 0.0
        self.process_val_lf_ff = 0.0
        self.process_val_lf_l = 0.0
        self.process_val_lf_u = 0.0
        # self.process_val_lf_x = 0.0
        self.process_val_rb_bf = 0.0
        self.process_val_rb_ff = 0.0
        self.process_val_rb_l = 0.0
        self.process_val_rb_m = 0.0
        self.process_val_rb_u = 0.0
        # self.process_val_rb_x = 0.0
        self.process_val_rf_bf = 0.0
        self.process_val_rf_ff = 0.0
        self.process_val_rf_l = 0.0
        self.process_val_rf_u = 0.0
        # self.process_val_rf_x = 0.0
        

        # Odom we only use it for the height detection and planar position ,
        #  because in real robots this data is not trivial.
        rospy.Subscriber("/t_bot_new/odom", Odometry, self.odom_callback)
        # We use the IMU for orientation and linearacceleration detection
        rospy.Subscriber("/tbot/imu/data", Imu, self.imu_callback)
        # We use it to get the contact force, to know if its in the air or stumping too hard.
        # rospy.Subscriber("/lowerleg_contactsensor_state", ContactsState, self.contact_callback)
        rospy.Subscriber("/t_bot_new/lb_bf_contactsensor_state", ContactsState, self.contact_callback_lb_bf)
        rospy.Subscriber("/t_bot_new/lb_ff_contactsensor_state", ContactsState, self.contact_callback_lb_ff)
        rospy.Subscriber("/t_bot_new/lf_bf_contactsensor_state", ContactsState, self.contact_callback_lf_bf)
        rospy.Subscriber("/t_bot_new/lf_ff_contactsensor_state", ContactsState, self.contact_callback_lf_ff)
        rospy.Subscriber("/t_bot_new/rb_bf_contactsensor_state", ContactsState, self.contact_callback_rb_bf)
        rospy.Subscriber("/t_bot_new/rb_ff_contactsensor_state", ContactsState, self.contact_callback_rb_ff)
        rospy.Subscriber("/t_bot_new/rf_bf_contactsensor_state", ContactsState, self.contact_callback_rf_bf)
        rospy.Subscriber("/t_bot_new/rf_ff_contactsensor_state", ContactsState, self.contact_callback_rf_ff)
        
        rospy.Subscriber("/t_bot_new/hip_joint_controller/state/process_value", Float64, self.process_val_callback_hip)
        rospy.Subscriber("/t_bot_new/lb_bf_joint_controller/state/process_value", Float64, self.process_val_callback_lb_bf)
        rospy.Subscriber("/t_bot_new/lb_ff_joint_controller/state/process_value", Float64, self.process_val_callback_lb_ff)
        rospy.Subscriber("/t_bot_new/lb_l_joint_controller/state/process_value", Float64, self.process_val_callback_lb_l)
        rospy.Subscriber("/t_bot_new/lb_m_joint_controller/state/process_value", Float64, self.process_val_callback_lb_m)
        rospy.Subscriber("/t_bot_new/lb_u_joint_controller/state/process_value", Float64, self.process_val_callback_lb_u)
        rospy.Subscriber("/t_bot_new/lb_x_joint_controller/state/process_value", Float64, self.process_val_callback_lb_x)

        rospy.Subscriber("/t_bot_new/lf_bf_joint_controller/state/process_value", Float64, self.process_val_callback_lf_bf)
        rospy.Subscriber("/t_bot_new/lf_ff_joint_controller/state/process_value", Float64, self.process_val_callback_lf_ff)
        rospy.Subscriber("/t_bot_new/lf_l_joint_controller/state/process_value", Float64, self.process_val_callback_lf_l)
        rospy.Subscriber("/t_bot_new/lf_u_joint_controller/state/process_value", Float64, self.process_val_callback_lf_u)
        rospy.Subscriber("/t_bot_new/lf_x_joint_controller/state/process_value", Float64, self.process_val_callback_lf_x)

        rospy.Subscriber("/t_bot_new/rb_bf_joint_controller/state/process_value", Float64, self.process_val_callback_rb_bf)
        rospy.Subscriber("/t_bot_new/rb_ff_joint_controller/state/process_value", Float64, self.process_val_callback_rb_ff)
        rospy.Subscriber("/t_bot_new/rb_l_joint_controller/state/process_value", Float64, self.process_val_callback_rb_l)
        rospy.Subscriber("/t_bot_new/rb_m_joint_controller/state/process_value", Float64, self.process_val_callback_rb_m)
        rospy.Subscriber("/t_bot_new/rb_u_joint_controller/state/process_value", Float64, self.process_val_callback_rb_u)
        rospy.Subscriber("/t_bot_new/rb_x_joint_controller/state/process_value", Float64, self.process_val_callback_rb_x)

        rospy.Subscriber("/t_bot_new/rf_bf_joint_controller/state/process_value", Float64, self.process_val_callback_rf_bf)
        rospy.Subscriber("/t_bot_new/rf_ff_joint_controller/state/process_value", Float64, self.process_val_callback_rf_ff)
        rospy.Subscriber("/t_bot_new/rf_l_joint_controller/state/process_value", Float64, self.process_val_callback_rf_l)
        rospy.Subscriber("/t_bot_new/rf_u_joint_controller/state/process_value", Float64, self.process_val_callback_rf_u)
        rospy.Subscriber("/t_bot_new/rf_x_joint_controller/state/process_value", Float64, self.process_val_callback_rf_x)
        # We use it to get the joints positions and calculate the reward associated to it
        # rospy.Subscriber("/t_bot_new/joint_states", JointState, self.joints_state_callback)

    def check_all_systems_ready(self):
        """
        We check that all systems are ready
        :return:
        """
        #print("tbot_state: check_all_systems_ready ")
        data_pose = None
        while data_pose is None and not rospy.is_shutdown():
            try:
                data_pose = rospy.wait_for_message("/t_bot_new/odom", Odometry, timeout=0.1)
                self.base_position = data_pose.pose.pose.position
                rospy.logdebug("Current odom READY")
                #print("Current odom READY")
            except:
                rospy.logdebug("Current odom pose not ready yet, retrying for getting robot base_position")
                #print("Current odom pose not ready yet, retrying for getting robot base_position")

        imu_data = None
        while imu_data is None and not rospy.is_shutdown():
            try:
                imu_data = rospy.wait_for_message("/tbot/imu/data", Imu, timeout=0.1)
                self.base_orientation = imu_data.orientation
                self.base_linear_acceleration = imu_data.linear_acceleration
                rospy.logdebug("Current imu_data READY")
                #print("Current imu_data READY")
            except:
                rospy.logdebug("Current imu_data not ready yet, retrying for getting robot base_orientation, and base_linear_acceleration")
                #print("Current imu_data not ready yet, retrying for getting robot base_orientation, and base_linear_acceleration")
        
        contacts_data_lb_bf = None
        while contacts_data_lb_bf is None and not rospy.is_shutdown():
            try:
                contacts_data_lb_bf = rospy.wait_for_message("/t_bot_new/lb_bf_contactsensor_state", ContactsState, timeout=0.1)
                for state in contacts_data_lb_bf.states:
                    self.contact_force_lb_bf = state.total_wrench.force
                rospy.logdebug("Current contacts_data_lb_bf READY")
                #print("Current contacts_data_lb_bf READY")
            except:
                rospy.logdebug("Current contacts_data_lb_bf not ready yet, retrying")
                #print("Current contacts_data_lb_bf not ready yet, retrying")
        
        contacts_data_lb_ff = None
        while contacts_data_lb_ff is None and not rospy.is_shutdown():
            try:
                contacts_data_lb_ff = rospy.wait_for_message("/t_bot_new/lb_ff_contactsensor_state", ContactsState, timeout=0.1)
                for state in contacts_data_lb_ff.states:
                    self.contact_force_lb_ff = state.total_wrench.force
                rospy.logdebug("Current contacts_data_lb_ff READY")
                #print("Current contacts_data_lb_ff READY")
            except:
                rospy.logdebug("Current contacts_data_lb_ff not ready yet, retrying")
                #print("Current contacts_data_lb_ff not ready yet, retrying")

        contacts_data_rb_bf = None
        while contacts_data_rb_bf is None and not rospy.is_shutdown():
            try:
                contacts_data_rb_bf = rospy.wait_for_message("/t_bot_new/rb_bf_contactsensor_state", ContactsState, timeout=0.1)
                for state in contacts_data_rb_bf.states:
                    self.contact_force_rb_bf = state.total_wrench.force
                rospy.logdebug("Current contacts_data_rb_bf READY")
                #print("Current contacts_data_rb_bf READY")
            except:
                rospy.logdebug("Current contacts_data_rb_bf not ready yet, retrying")
                #print("Current contacts_data_rb_bf not ready yet, retrying")
        
        contacts_data_rb_ff = None
        while contacts_data_rb_ff is None and not rospy.is_shutdown():
            try:
                contacts_data_rb_ff = rospy.wait_for_message("/t_bot_new/rb_ff_contactsensor_state", ContactsState, timeout=0.1)
                for state in contacts_data_rb_ff.states:
                    self.contact_force_rb_ff = state.total_wrench.force
                rospy.logdebug("Current contacts_data_rb_ff READY")
                #print("Current contacts_data_rb_ff READY")
            except:
                rospy.logdebug("Current contacts_data_rb_ff not ready yet, retrying")
                #print("Current contacts_data_rb_ff not ready yet, retrying")

        contacts_data_rf_bf = None
        while contacts_data_rf_bf is None and not rospy.is_shutdown():
            try:
                contacts_data_rf_bf = rospy.wait_for_message("/t_bot_new/rf_bf_contactsensor_state", ContactsState, timeout=0.1)
                for state in contacts_data_rf_bf.states:
                    self.contact_force_rf_bf = state.total_wrench.force
                rospy.logdebug("Current contacts_data_rf_bf READY")
                #print("Current contacts_data_rf_bf READY")
            except:
                rospy.logdebug("Current contacts_data_rf_bf not ready yet, retrying")
                #print("Current contacts_data_rf_bf not ready yet, retrying")
        
        contacts_data_rf_ff = None
        while contacts_data_rf_ff is None and not rospy.is_shutdown():
            try:
                contacts_data_rf_ff = rospy.wait_for_message("/t_bot_new/rf_ff_contactsensor_state", ContactsState, timeout=0.1)
                for state in contacts_data_rf_ff.states:
                    self.contact_force_rf_ff = state.total_wrench.force
                rospy.logdebug("Current contacts_data_rf_ff READY")
                #print("Current contacts_data_rf_ff READY")
            except:
                rospy.logdebug("Current contacts_data_rf_ff not ready yet, retrying")
                #print("Current contacts_data_rf_ff not ready yet, retrying")

        # joint_states_msg = None
        # while joint_states_msg is None and not rospy.is_shutdown():
        #     try:
        #         joint_states_msg = rospy.wait_for_message("/t_bot_new/joint_states", JointState, timeout=0.1)
        #         self.joints_state = joint_states_msg
        #         rospy.logdebug("Current joint_states READY")
        #         #print("Current joint_states READY")
        #     except Exception as e:
        #         rospy.logdebug("Current joint_states not ready yet, retrying==>"+str(e))
        #         #print("Current joint_states not ready yet, retrying==>"+str(e))

        rospy.logdebug("ALL SYSTEMS READY")
        #print("ALL SYSTEMS READY")

    def set_desired_world_point(self, x, y, z):
        """
        Point where you want the tbot to be
        :return:
        """
        self.desired_world_point.x = x
        self.desired_world_point.y = y
        self.desired_world_point.z = z


    def get_base_height(self):
        return self.base_position.z

    def get_base_rpy(self):
        euler_rpy = Vector3()
        euler = tf.transformations.euler_from_quaternion(
            [self.base_orientation.x, self.base_orientation.y, self.base_orientation.z, self.base_orientation.w])

        euler_rpy.x = euler[0]
        euler_rpy.y = euler[1]
        euler_rpy.z = euler[2]
        return euler_rpy

    def get_distance_from_point(self, p_end):
        """
        Given a Vector3 Object, get distance from current position
        :param p_end:
        :return:
        """
        a = numpy.array((self.base_position.x, self.base_position.y, self.base_position.z))
        b = numpy.array((p_end.x, p_end.y, p_end.z))

        distance = numpy.linalg.norm(a - b)

        return distance

    def get_contact_force_magnitude(self):
        """
        You will see that because the X axis is the one pointing downwards, it will be the one with
        higher value when touching the floor
        For a Robot of total mas of 0.55Kg, a gravity of 9.81 m/sec**2, Weight = 0.55*9.81=5.39 N
        Falling from around 5centimetres ( negligible height ), we register peaks around
        Fx = 7.08 N
        :return:
        """
        # contact_force = self.contact_force
        contact_force_lb_bf = self.contact_force_lb_bf
        contact_force_lb_ff = self.contact_force_lb_ff
        contact_force_lf_bf = self.contact_force_lf_bf
        contact_force_lf_ff = self.contact_force_lf_ff
        contact_force_rb_bf = self.contact_force_rb_bf
        contact_force_rb_ff = self.contact_force_rb_ff
        contact_force_rf_bf = self.contact_force_rf_bf
        contact_force_rf_ff = self.contact_force_rf_ff
        contact_force_np = numpy.array((contact_force_lb_bf.x + contact_force_lb_ff.x + contact_force_lf_bf.x + contact_force_lf_ff.x + contact_force_rb_bf.x + contact_force_rb_ff.x + contact_force_rf_bf.x + contact_force_rf_ff.x, 
                                        contact_force_lb_bf.y + contact_force_lb_ff.y + contact_force_lf_bf.y + contact_force_lf_ff.y + contact_force_rb_bf.y + contact_force_rb_ff.y + contact_force_rf_bf.y + contact_force_rf_ff.y,
                                        contact_force_lb_bf.z + contact_force_lb_ff.z + contact_force_lf_bf.z + contact_force_lf_ff.z + contact_force_rb_bf.z + contact_force_rb_ff.z + contact_force_rf_bf.z + contact_force_rf_ff.z))
        
        # (contact_force_lb_bf.x, contact_force_lb_bf.y, contact_force_lb_bf.z,
        # contact_force_lb_ff.x, contact_force_lb_ff.y, contact_force_lb_ff.z,
        # contact_force_lf_bf.x, contact_force_lf_bf.y, contact_force_lf_bf.z,
        # contact_force_lf_ff.x, contact_force_lf_ff.y, contact_force_lf_ff.z,
        # contact_force_rb_bf.x, contact_force_rb_bf.y, contact_force_rb_bf.z,
        # contact_force_rb_ff.x, contact_force_rb_ff.y, contact_force_rb_ff.z,
        # contact_force_rf_bf.x, contact_force_rf_bf.y, contact_force_rf_bf.z,
        # contact_force_rf_ff.x, contact_force_rf_ff.y, contact_force_rf_ff.z)
        
        force_magnitude = numpy.linalg.norm(contact_force_np)

        return force_magnitude

    # def get_joint_states(self):
    #     j_state =  [self.process_val_hip,self.process_val_lb_bf,self.process_val_lb_ff,self.process_val_lb_l,self.process_val_lb_m,self.process_val_lb_u,self.process_val_lb_x,
    #             self.process_val_lf_bf,self.process_val_lf_ff,self.process_val_lf_l,self.process_val_lf_u,self.process_val_lf_x,
    #             self.process_val_rb_bf,self.process_val_rb_ff,self.process_val_rb_l,self.process_val_rb_m,self.process_val_rb_u,self.process_val_rb_x,
    #             self.process_val_rf_bf,self.process_val_rf_ff,self.process_val_rf_l,self.process_val_rf_u,self.process_val_rf_x]
    #     ##print("j_state type:"+ str(type(j_state)))
    #     ##print("j_state :"+ str(j_state))
    #     return j_state

    def get_joint_states(self):
        j_state =  [self.process_val_lb_bf,self.process_val_lb_ff,self.process_val_lb_l,self.process_val_lb_m,self.process_val_lb_u,
                    self.process_val_lf_bf,self.process_val_lf_ff,self.process_val_lf_l,self.process_val_lf_u,
                    self.process_val_rb_bf,self.process_val_rb_ff,self.process_val_rb_l,self.process_val_rb_m,self.process_val_rb_u,
                    self.process_val_rf_bf,self.process_val_rf_ff,self.process_val_rf_l,self.process_val_rf_u]
        ##print("j_state type:"+ str(type(j_state)))
        ##print("j_state :"+ str(j_state))
        return j_state
    def odom_callback(self,msg):
        self.base_position = msg.pose.pose.position

    def imu_callback(self,msg):
        self.base_orientation = msg.orientation
        self.base_linear_acceleration = msg.linear_acceleration

    def contact_callback_lb_bf(self,msg):
        """
        /lowerleg_contactsensor_state/states[0]/contact_positions ==> PointContact in World
        /lowerleg_contactsensor_state/states[0]/contact_normals ==> NormalContact in World

        ==> One is an array of all the forces, the other total,
         and are relative to the contact link referred to in the sensor.
        /lowerleg_contactsensor_state/states[0]/wrenches[]
        /lowerleg_contactsensor_state/states[0]/total_wrench
        :param msg:
        :return:
        """
        for state in msg.states:
            self.contact_force_lb_bf = state.total_wrench.force
        
    def contact_callback_lb_ff(self,msg):      
        for state in msg.states:
            self.contact_force_lb_ff = state.total_wrench.force

    def contact_callback_lf_bf(self,msg):      
        for state in msg.states:
            self.contact_force_lf_bf = state.total_wrench.force

    def contact_callback_lf_ff(self,msg):     
        for state in msg.states:
            self.contact_force_lf_ff = state.total_wrench.force

    def contact_callback_rb_bf(self,msg):      
        for state in msg.states:
            self.contact_force_rb_bf = state.total_wrench.force
    
    def contact_callback_rb_ff(self,msg):      
        for state in msg.states:
            self.contact_force_rb_ff = state.total_wrench.force

    def contact_callback_rf_bf(self,msg):       
        for state in msg.states:
            self.contact_force_rf_bf = state.total_wrench.force

    def contact_callback_rf_ff(self,msg):
        for state in msg.states:
            self.contact_force_rf_ff = state.total_wrench.force

    def joints_state_callback(self,msg):
        self.joints_state = msg

    # def process_val_callback_hip(self,msg):
    #     self.process_val_hip = msg

    def process_val_callback_lb_bf(self,msg):
        self.process_val_lb_bf = msg

    def process_val_callback_lb_ff(self,msg):
        self.process_val_lb_ff = msg
    
    def process_val_callback_lb_l(self,msg):
        self.process_val_lb_l = msg

    def process_val_callback_lb_m(self,msg):
        self.process_val_lb_m = msg

    def process_val_callback_lb_u(self,msg):
        self.process_val_lb_u = msg

    # def process_val_callback_lb_x(self,msg):
    #     self.process_val_lb_x = msg

    def process_val_callback_lf_bf(self,msg):
        self.process_val_lf_bf = msg

    def process_val_callback_lf_ff(self,msg):
        self.process_val_lf_ff = msg
    
    def process_val_callback_lf_l(self,msg):
        self.process_val_lf_l = msg

    def process_val_callback_lf_u(self,msg):
        self.process_val_lf_u = msg

    # def process_val_callback_lf_x(self,msg):
    #     self.process_val_lf_x = msg

    def process_val_callback_rb_bf(self,msg):
        self.process_val_rb_bf = msg

    def process_val_callback_rb_ff(self,msg):
        self.process_val_rb_ff = msg
    
    def process_val_callback_rb_l(self,msg):
        self.process_val_rb_l = msg

    def process_val_callback_rb_m(self,msg):
        self.process_val_rb_m = msg

    def process_val_callback_rb_u(self,msg):
        self.process_val_rb_u = msg

    # def process_val_callback_rb_x(self,msg):
    #     self.process_val_rb_x = msg

    def process_val_callback_rf_bf(self,msg):
        self.process_val_rf_bf = msg

    def process_val_callback_rf_ff(self,msg):
        self.process_val_rf_ff = msg
    
    def process_val_callback_rf_l(self,msg):
        self.process_val_rf_l = msg

    def process_val_callback_rf_u(self,msg):
        self.process_val_rf_u = msg

    # def process_val_callback_rf_x(self,msg):
    #     self.process_val_rf_x = msg

    def tbot_height_ok(self):
        # print('height_ok :  ' +str(self.get_base_height()))
        height_ok = self._min_height <= self.get_base_height() < self._max_height
        # print('height_ok: '+str(height_ok))
        return height_ok

    def tbot_orientation_ok(self):
        
        orientation_rpy = self.get_base_rpy()
        x_or = abs(orientation_rpy.x)
        y_or = abs(orientation_rpy.y)
        # print('abs(orientation_rpy.x) :  ' +str(x_or))
        # print('abs(orientation_rpy.y) :  ' +str(y_or))
        roll_ok = self._abs_max_roll > x_or
        pitch_ok = self._abs_max_pitch > y_or
        orientation_ok = roll_ok and pitch_ok
        # print('orientation_ok: '+str(orientation_ok))
        return orientation_ok
        

    def calculate_reward_joint_position(self, weight=1.0):
        """
        We calculate reward base on the joints configuration. The more near 0 the better.
        :return:
        """
        acumulated_joint_pos = 0.0
        j_states = self.get_joint_states()
        for joint_pos in j_states:
            # Abs to remove sign influence, it doesnt matter the direction of turn.
            acumulated_joint_pos += abs(joint_pos)
            rospy.logdebug("calculate_reward_joint_position>>acumulated_joint_pos=" + str(acumulated_joint_pos))
        reward = weight * acumulated_joint_pos
        rospy.logdebug("calculate_reward_joint_position>>reward=" + str(reward))
        return reward

    def calculate_reward_joint_effort(self, weight=1.0):
        """
        We calculate reward base on the joints effort readings. The more near 0 the better.
        :return:
        """
        acumulated_joint_effort = 0.0
        for joint_effort in self.joints_state.effort:
            # Abs to remove sign influence, it doesnt matter the direction of the effort.
            acumulated_joint_effort += abs(joint_effort)
            rospy.logdebug("calculate_reward_joint_effort>>joint_effort=" + str(joint_effort))
            rospy.logdebug("calculate_reward_joint_effort>>acumulated_joint_effort=" + str(acumulated_joint_effort))
        reward = weight * acumulated_joint_effort
        rospy.logdebug("calculate_reward_joint_effort>>reward=" + str(reward))
        return reward

    def calculate_reward_contact_force(self, weight=1.0):
        """
        We calculate reward base on the contact force.
        The nearest to the desired contact force the better.
        We use exponential to magnify big departures from the desired force.
        Default ( 7.08 N ) desired force was taken from reading of the robot touching
        the ground from a negligible height of 5cm.
        :return:
        """
        force_magnitude = self.get_contact_force_magnitude()
        force_displacement = force_magnitude - self._desired_force

        rospy.logdebug("calculate_reward_contact_force>>force_magnitude=" + str(force_magnitude))
        rospy.logdebug("calculate_reward_contact_force>>force_displacement=" + str(force_displacement))
        # Abs to remove sign
        reward = weight * abs(force_displacement)
        rospy.logdebug("calculate_reward_contact_force>>reward=" + str(reward))
        return reward

    def calculate_reward_orientation(self, weight=1.0):
        """
        We calculate the reward based on the orientation.
        The more its closser to 0 the better because it means its upright
        desired_yaw is the yaw that we want it to be.
        to praise it to have a certain orientation, here is where to set it.
        :return:
        """
        curren_orientation = self.get_base_rpy()
        yaw_displacement = curren_orientation.z - self._desired_yaw
        rospy.logdebug("calculate_reward_orientation>>[R,P,Y]=" + str(curren_orientation))
        acumulated_orientation_displacement = abs(curren_orientation.x) + abs(curren_orientation.y) + abs(yaw_displacement)
        reward = weight * acumulated_orientation_displacement
        rospy.logdebug("calculate_reward_orientation>>reward=" + str(reward))
        return reward

    def calculate_reward_distance_from_des_point(self, weight=1.0):
        """
        We calculate the distance from the desired point.
        The closser the better
        :param weight:
        :return:reward
        """
        distance = self.get_distance_from_point(self.desired_world_point)
        reward = weight * distance
        rospy.logdebug("calculate_reward_orientation>>reward=" + str(reward))
        return reward

    def calculate_total_reward(self):
        """
        We consider VERY BAD REWARD -7 or less
        Perfect reward is 0.0, and total reward 1.0.
        The defaults values are chosen so that when the robot has fallen or very extreme joint config:
        r1 = -8.04
        r2 = -8.84
        r3 = -7.08
        r4 = -10.0 ==> We give priority to this, giving it higher value.
        :return:
        """

        r1 = self.calculate_reward_joint_position(self._weight_r1)
        r2 = self.calculate_reward_joint_effort(self._weight_r2)
        # Desired Force in Newtons, taken form idle contact with 9.81 gravity.
        r3 = self.calculate_reward_contact_force(self._weight_r3)
        r4 = self.calculate_reward_orientation(self._weight_r4)
        r5 = self.calculate_reward_distance_from_des_point(self._weight_r5)

        # The sign depend on its function.
        total_reward = self._alive_reward - r1 - r2 - r3 - r4 - r5

        rospy.logdebug("###############")
        rospy.logdebug("alive_bonus=" + str(self._alive_reward))
        rospy.logdebug("r1 joint_position=" + str(r1))
        rospy.logdebug("r2 joint_effort=" + str(r2))
        rospy.logdebug("r3 contact_force=" + str(r3))
        rospy.logdebug("r4 orientation=" + str(r4))
        rospy.logdebug("r5 distance=" + str(r5))
        rospy.logdebug("total_reward=" + str(total_reward))
        rospy.logdebug("###############")
        # #print("total_reward: "+str(total_reward))
        return total_reward




    def get_observations(self):
        """
        Returns the state of the robot needed for OpenAI QLearn Algorithm
        The state will be defined by an array of the:
        1) distance from desired point in meters
        2) The pitch orientation in radians
        3) the Roll orientation in radians
        4) the Yaw orientation in radians
        5) Force in contact sensor in Newtons
        6-7-8) State of the 23 joints in radians

        observation = ["distance_from_desired_point",
                 "base_roll",
                 "base_pitch",
                 "base_yaw",
                 "contact_force",
                 "joint_states_hip","joint_states_lb_bf","joint_states_lb_ff","joint_states_lb_l","joint_states_lb_m","joint_states_lb_u","joint_states_lb_x",
                 "joint_states_lf_bf","joint_states_lf_ff","joint_states_lf_l","joint_states_lf_u","joint_states_lf_x",
                 "joint_states_rb_bf","joint_states_rb_ff","joint_states_rb_l","joint_states_rb_m","joint_states_rb_u","joint_states_rb_x",
                 "joint_states_rf_bf","joint_states_rf_ff","joint_states_rf_l","joint_states_rf_u","joint_states_rf_x"]

        :return: observation
        """
        # #print("in get_observations")
        distance_from_desired_point = self.get_distance_from_point(self.desired_world_point)

        base_orientation = self.get_base_rpy()
        base_roll = base_orientation.x
        base_pitch = base_orientation.y
        base_yaw = base_orientation.z

        contact_force = self.get_contact_force_magnitude()
        """
        name:  [hip_joint, lb_bf_joint, lb_ff_joint, lb_l_joint, lb_m_joint, lb_u_joint, lb_x_joint,
                lf_bf_joint, lf_ff_joint, lf_l_joint, lf_u_joint, lf_x_joint, rb_bf_joint, rb_ff_joint,
                rb_l_joint, rb_m_joint, rb_u_joint, rb_x_joint, rf_bf_joint, rf_ff_joint, rf_l_joint,
                rf_u_joint, rf_x_joint]

        """
        joint_states_position = self.get_joint_states()
        ##print("joint_states")
        joint_states_hip    =   joint_states_position[0] 
        joint_states_lb_bf  =   joint_states_position[1] 
        joint_states_lb_ff  =   joint_states_position[2] 
        joint_states_lb_l   =   joint_states_position[3] 
        joint_states_lb_m   =   joint_states_position[4] 
        joint_states_lb_u   =   joint_states_position[5] 
        joint_states_lb_x   =   joint_states_position[6] 
        joint_states_lf_bf  =   joint_states_position[7] 
        joint_states_lf_ff  =   joint_states_position[8] 
        joint_states_lf_l   =   joint_states_position[9] 
        joint_states_lf_u   =   joint_states_position[10] 
        joint_states_lf_x   =   joint_states_position[11] 
        joint_states_rb_bf  =   joint_states_position[12] 
        joint_states_rb_ff  =   joint_states_position[13] 
        joint_states_rb_l   =   joint_states_position[14] 
        joint_states_rb_m   =   joint_states_position[15] 
        joint_states_rb_u   =   joint_states_position[16] 
        joint_states_rb_x   =   joint_states_position[17] 
        joint_states_rf_bf  =   joint_states_position[18] 
        joint_states_rf_ff  =   joint_states_position[19] 
        joint_states_rf_l   =   joint_states_position[20] 
        joint_states_rf_u   =   joint_states_position[21] 
        joint_states_rf_x   =   joint_states_position[22]

        observation = []
        for obs_name in self._list_of_observations:
            if obs_name == "distance_from_desired_point":
                observation.append(distance_from_desired_point)
            elif obs_name == "base_roll":
                observation.append(base_roll)
            elif obs_name == "base_pitch":
                observation.append(base_pitch)
            elif obs_name == "base_yaw":
                observation.append(base_yaw)
            elif obs_name == "contact_force":
                observation.append(contact_force)
            # elif obs_name == "joint_states_hip":
            #     observation.append(joint_states_hip)
            elif obs_name == "joint_states_lb_bf":
                observation.append(joint_states_lb_bf)
            elif obs_name == "joint_states_lb_ff":
                observation.append(joint_states_lb_ff)
            elif obs_name == "joint_states_lb_l":
                observation.append(joint_states_lb_l)
            elif obs_name == "joint_states_lb_m":
                observation.append(joint_states_lb_m)
            elif obs_name == "joint_states_lb_u":
            #     observation.append(joint_states_lb_u)
            # elif obs_name == "joint_states_lb_x":
            #     observation.append(joint_states_lb_x)
            elif obs_name == "joint_states_lf_bf":
                observation.append(joint_states_lf_bf)
            elif obs_name == "joint_states_lf_ff":
                observation.append(joint_states_lf_ff)
            elif obs_name == "joint_states_lf_l":
                observation.append(joint_states_lf_l)
            elif obs_name == "joint_states_lf_u":
                observation.append(joint_states_lf_u)
            # elif obs_name == "joint_states_lf_x":
            #     observation.append(joint_states_lf_x)
            elif obs_name == "joint_states_rb_bf":
                observation.append(joint_states_rb_bf)
            elif obs_name == "joint_states_rb_ff":
                observation.append(joint_states_rb_ff)
            elif obs_name == "joint_states_rb_l":
                observation.append(joint_states_rb_l)
            elif obs_name == "joint_states_rb_m":
                observation.append(joint_states_rb_m)
            elif obs_name == "joint_states_rb_u":
                observation.append(joint_states_rb_u)
            # elif obs_name == "joint_states_rb_x":
            #     observation.append(joint_states_rb_x)
            elif obs_name == "joint_states_rf_bf":
                observation.append(joint_states_rf_bf)
            elif obs_name == "joint_states_rf_ff":
                observation.append(joint_states_rf_ff)
            elif obs_name == "joint_states_rf_l":
                observation.append(joint_states_rf_l)
            elif obs_name == "joint_states_rf_u":
                observation.append(joint_states_rf_u)
            # elif obs_name == "joint_states_rf_x":
            #     observation.append(joint_states_rf_x)
            else:
                raise NameError('Observation Asked does not exist=='+str(obs_name))

        return observation

    def get_state_as_string(self, observation):
        """
        This function will do two things:
        1) It will make discrete the observations
        2) Will convert the discrete observations in to state tags strings
        :param observation:
        :return: state
        """
        observations_discrete = self.assign_bins(observation)
        string_state = ''.join(map(str, observations_discrete))
        return observation

    def assign_bins(self, observation):
        """
        Will make observations discrete by placing each value into its corresponding bin
        :param observation:
        :return:
        """
        state_discrete = numpy.zeros(len(self._list_of_observations))
        for i in range(len(self._list_of_observations)):
            state_discrete[i] = numpy.digitize(observation[i], self._bins[i])
        return state_discrete

    def init_bins(self):
        """
        We initalise all related to the bins
        :return:
        """
        self.fill_observations_ranges()
        self.create_bins()

    def fill_observations_ranges(self):
        """
        We create the dictionary for the ranges of the data related to each observation
        :return:
        """
        self._obs_range_dict = {}
        for obs_name in self._list_of_observations:

            if obs_name == "distance_from_desired_point":
                # We consider the range as based on the range of distance allowed in height
                delta = self._max_height - self._min_height
                max_value = delta
                min_value = -delta
            elif obs_name == "base_roll":
                max_value = self._abs_max_roll
                min_value = -self._abs_max_roll
            elif obs_name == "base_pitch":
                max_value = self._abs_max_pitch
                min_value = -self._abs_max_pitch
            elif obs_name == "base_yaw":
                # We consider that 360 degrees is max range
                max_value = 2*math.pi
                min_value = -2*math.pi
            elif obs_name == "contact_force":
                # We consider that no force is the minimum, and the maximum is 2 times the desired
                # We dont want to make a very big range because we might loose the desired force
                # in the middle.
                max_value = 2*self._desired_force
                min_value = 0.0
            # elif obs_name == "joint_states_hip":
            #     # We consider the URDF maximum values
            #     max_value = 0.0
            #     min_value = 0.0
            elif obs_name == "joint_states_lb_bf":
                max_value = 2.5
                min_value = 0.0
            elif obs_name == "joint_states_lb_ff":
                max_value = 0.0
                min_value = -3.0
            elif obs_name == "joint_states_lb_l":
                max_value = 0.0
                min_value = -1.4
            elif obs_name == "joint_states_lb_m":
                max_value = 0.2
                min_value = -1.5
            elif obs_name == "joint_states_lb_u":
                max_value = 0.75
                min_value = -0.75
            # elif obs_name == "joint_states_lb_x":
            #     max_value = 0.0
            #     min_value = 0.0
            elif obs_name == "joint_states_lf_bf":
                max_value = 2.8
                min_value = 0.0
            elif obs_name == "joint_states_lf_ff":
                max_value = 0.0
                min_value = -2.5
            elif obs_name == "joint_states_lf_l":
                max_value = 0.0
                min_value = -1.0
            elif obs_name == "joint_states_lf_u":
                max_value = 0.75
                min_value = -0.75
            # elif obs_name == "joint_states_lf_x":
            #     max_value = 0.0
            #     min_value = 0.0
            elif obs_name == "joint_states_rb_bf":
                max_value = 2.5
                min_value = 0.0
            elif obs_name == "joint_states_rb_ff":
                max_value = 0.0
                min_value = -3.0
            elif obs_name == "joint_states_rb_l":
                max_value = 0.0
                min_value = -1.4
            elif obs_name == "joint_states_rb_m":
                max_value = 0.2
                min_value = -1.5
            elif obs_name == "joint_states_rb_u":
                max_value = 0.75
                min_value = -0.75
            # elif obs_name == "joint_states_rb_x":
            #     max_value = 0.0
            #     min_value = 0.0
            elif obs_name == "joint_states_rf_bf":
                max_value = 2.8
                min_value = 0.0
            elif obs_name == "joint_states_rf_ff":
                max_value = 0.0
                min_value = -2.5
            elif obs_name == "joint_states_rf_l":
                max_value = 0.0
                min_value = -1.0
            elif obs_name == "joint_states_rf_u":
                max_value = 0.75
                min_value = -0.75
            # elif obs_name == "joint_states_rf_x":
            #     max_value = 0.0
            #     min_value = 0.0
            else:
                raise NameError('Observation Asked does not exist=='+str(obs_name))

            self._obs_range_dict[obs_name] = [min_value,max_value]

    def create_bins(self):
        """
        We create the Bins for the discretization of the observations
        self.desired_world_point = Vector3(0.0, 0.0, 0.0)
        self._min_height = min_height
        self._max_height = max_height
        self._abs_max_roll = abs_max_roll
        self._abs_max_pitch = abs_max_pitch
        self._joint_increment_value = joint_increment_value
        self._done_reward = done_reward
        self._alive_reward = alive_reward
        self._desired_force = desired_force
        self._desired_yaw = desired_yaw


        :return:bins
        """

        number_of_observations = len(self._list_of_observations)
        parts_we_disrcetize = self._discrete_division

        self._bins = numpy.zeros((number_of_observations, parts_we_disrcetize))
        for counter in range(number_of_observations):
            obs_name = self._list_of_observations[counter]
            min_value = self._obs_range_dict[obs_name][0]
            max_value = self._obs_range_dict[obs_name][1]
            self._bins[counter] = numpy.linspace(min_value, max_value, parts_we_disrcetize)


    def get_action_to_position(self, action):
        """
        Here we have the ACtions number to real joint movement correspondance.
        :param action: Integer that goes from 0 to 5, because we have 6 actions.
        :return:
        """
        # We get current Joints values
        joint_states_position = self.get_joint_states()
        # joint_states_position = joint_states.position
        #print("action: ")
        #print(action)
        #print("joint_states_position: ")
        #print(joint_states_position)
        # action_position = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        action_position = joint_states_position
        rospy.logdebug("get_action_to_position>>>"+str(joint_states_position))

        return action, joint_states_position

    def get_action_to_position_1(self, action):
        """
        Here we have the ACtions number to real joint movement correspondance.
        :param action: Integer that goes from 0 to 5, because we have 6 actions.
        :return:
        """
        # We get current Joints values
        joint_states_position = self.get_joint_states()
        # joint_states_position = joint_states.position
        #print("action_1111111111: " + str(action))
        
        #print("joint_states_position_1111111111: " + str(joint_states_position))
        
        # action_position = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        action_position = joint_states_position
        rospy.logdebug("get_action_to_position>>>"+str(joint_states_position))
        if action == 0: #Increment haa_joint
            action_position[0] = joint_states_position[0] + self._joint_increment_value
        elif action == 1: #Decrement haa_joint
            action_position[0] = joint_states_position[0] - self._joint_increment_value
        elif action == 2: #Increment hfe_joint
            action_position[1] = joint_states_position[1] + self._joint_increment_value
        elif action == 3: #Decrement hfe_joint
            action_position[1] = joint_states_position[1] - self._joint_increment_value
        elif action == 4: #Increment kfe_joint
            action_position[2] = joint_states_position[2] + self._joint_increment_value
        elif action == 5:  #Decrement kfe_joint
            action_position[2] = joint_states_position[2] - self._joint_increment_value
        elif action == 6: #Decrement haa_joint
            action_position[3] = joint_states_position[3] + self._joint_increment_value
        elif action == 7: #Decrement haa_joint
            action_position[3] = joint_states_position[3] - self._joint_increment_value
        elif action == 8: #Increment hfe_joint
            action_position[4] = joint_states_position[4] + self._joint_increment_value
        elif action == 9: #Decrement hfe_joint
            action_position[4] = joint_states_position[4] - self._joint_increment_value
        elif action == 10: #Increment kfe_joint
            action_position[5] = joint_states_position[5] + self._joint_increment_value
        elif action == 11:  #Decrement kfe_joint
            action_position[5] = joint_states_position[5] - self._joint_increment_value
        elif action == 12: #Increment hfe_joint
            action_position[6] = joint_states_position[6] + self._joint_increment_value
        elif action == 13: #Decrement hfe_joint
            action_position[6] = joint_states_position[6] - self._joint_increment_value
        elif action == 14: #Increment kfe_joint
            action_position[7] = joint_states_position[7] + self._joint_increment_value
        elif action == 15:  #Decrement kfe_joint
            action_position[7] = joint_states_position[7] - self._joint_increment_value
        elif action == 16: #Decrement haa_joint
            action_position[8] = joint_states_position[8] + self._joint_increment_value
        elif action == 17: #Decrement haa_joint
            action_position[8] = joint_states_position[8] - self._joint_increment_value
        elif action == 18: #Increment hfe_joint
            action_position[9] = joint_states_position[9] + self._joint_increment_value
        elif action == 19: #Decrement hfe_joint
            action_position[9] = joint_states_position[9] - self._joint_increment_value
        elif action == 20: #Increment kfe_joint
            action_position[10] = joint_states_position[10] + self._joint_increment_value
        elif action == 21:  #Decrement kfe_joint
            action_position[10] = joint_states_position[10] - self._joint_increment_value
        elif action == 22: #Increment hfe_joint
            action_position[11] = joint_states_position[11] + self._joint_increment_value
        elif action == 23: #Decrement hfe_joint
            action_position[11] = joint_states_position[11] - self._joint_increment_value
        elif action == 24: #Increment kfe_joint
            action_position[12] = joint_states_position[12] + self._joint_increment_value
        elif action == 25:  #Decrement kfe_joint
            action_position[12] = joint_states_position[12] - self._joint_increment_value
        elif action == 26: #Decrement haa_joint
            action_position[13] = joint_states_position[13] + self._joint_increment_value
        elif action == 27: #Decrement haa_joint
            action_position[13] = joint_states_position[13] - self._joint_increment_value
        elif action == 28: #Increment hfe_joint
            action_position[14] = joint_states_position[14] + self._joint_increment_value
        elif action == 29: #Decrement hfe_joint
            action_position[14] = joint_states_position[14] - self._joint_increment_value
        elif action == 30: #Increment kfe_joint
            action_position[15] = joint_states_position[15] + self._joint_increment_value
        elif action == 31:  #Decrement kfe_joint
            action_position[15] = joint_states_position[15] - self._joint_increment_value
        elif action == 32: #Increment hfe_joint
            action_position[16] = joint_states_position[16] + self._joint_increment_value
        elif action == 33: #Decrement hfe_joint
            action_position[16] = joint_states_position[16] - self._joint_increment_value
        elif action == 34: #Increment kfe_joint
            action_position[17] = joint_states_position[17] + self._joint_increment_value
        elif action == 35:  #Decrement kfe_joint
            action_position[17] = joint_states_position[17] - self._joint_increment_value
        elif action == 36: #Decrement haa_joint
            action_position[18] = joint_states_position[18] + self._joint_increment_value
        elif action == 37: #Decrement haa_joint
            action_position[18] = joint_states_position[18] - self._joint_increment_value
        elif action == 38: #Increment hfe_joint
            action_position[19] = joint_states_position[19] + self._joint_increment_value
        elif action == 39: #Decrement hfe_joint
            action_position[19] = joint_states_position[19] - self._joint_increment_value
        elif action == 40: #Increment kfe_joint
            action_position[20] = joint_states_position[20] + self._joint_increment_value
        elif action == 41:  #Decrement kfe_joint
            action_position[20] = joint_states_position[20] - self._joint_increment_value
        elif action == 42: #Increment hfe_joint
            action_position[21] = joint_states_position[21] + self._joint_increment_value
        elif action == 43: #Decrement hfe_joint
            action_position[21] = joint_states_position[21] - self._joint_increment_value
        elif action == 44: #Increment kfe_joint
            action_position[22] = joint_states_position[22] + self._joint_increment_value
        elif action == 45:  #Decrement kfe_joint
            action_position[22] = joint_states_position[22] - self._joint_increment_value
        else:
            action_position = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]


        #print("action_position_1111111111: " + str(action_position))
        
        return action_position

    def process_data(self):
        """
        We return the total reward based on the state in which we are in and if its done or not
        ( it fell basically )
        :return: reward, done
        """
        tbot_height_ok = self.tbot_height_ok()
        tbot_orientation_ok = self.tbot_orientation_ok()

        done = not(tbot_height_ok and tbot_orientation_ok)
        # print('process_data')
        # print(tbot_height_ok)
        # print(tbot_orientation_ok)
        # print(done)
        if done:
            # print("process_data_1091_state: Done")
            rospy.logdebug("It fell, so the reward has to be very low")
            total_reward = self._done_reward - 20
        else:
            rospy.logdebug("Calculate normal reward because it didn't fall.")
            total_reward = self.calculate_total_reward()+ 20

        return total_reward, done

    def testing_loop(self):
        rate = rospy.Rate(50)
        while not rospy.is_shutdown():
            #print("in main loop 3")
            self.calculate_total_reward()
            rate.sleep()


if __name__ == "__main__":
    rospy.init_node('tbot_state_node', anonymous=True)
    tbot_state = TbotState(max_height=0.8,
                                 min_height=0.2,
                                 abs_max_roll=1.57,
                                 abs_max_pitch=1.57)
    tbot_state.testing_loop()


















# switch(action) {
        #     case 0: action_position[0] = joint_states_position[0] + self._joint_increment_value;
        #             break;
        #     case 1: action_position[0] = joint_states_position[0] - self._joint_increment_value;
        #             break;
        #     case 2: action_position[1] = joint_states_position[1] + self._joint_increment_value;
        #             break;
        #     case 3: action_position[1] = joint_states_position[1] - self._joint_increment_value;
        #             break;
        #     case 4: action_position[2] = joint_states_position[2] + self._joint_increment_value;
        #             break;
        #     case 5: action_position[2] = joint_states_position[2] - self._joint_increment_value;
        #             break;
        #     case 6: action_position[3] = joint_states_position[3] + self._joint_increment_value;
        #             break;
        #     case 7: action_position[3] = joint_states_position[3] - self._joint_increment_value;
        #             break;
        #     case 8: action_position[4] = joint_states_position[4] + self._joint_increment_value;
        #             break;
        #     case 9: action_position[4] = joint_states_position[4] - self._joint_increment_value;
        #             break;
        #     case 10: action_position[5] = joint_states_position[5] + self._joint_increment_value;
        #             break;
        #     case 11: action_position[5] = joint_states_position[5] - self._joint_increment_value;
        #             break;
        #     case 12: action_position[6] = joint_states_position[6] + self._joint_increment_value;
        #             break;
        #     case 13: action_position[6] = joint_states_position[6] - self._joint_increment_value;
        #             break;
        #     case 14: action_position[7] = joint_states_position[7] + self._joint_increment_value;
        #             break;
        #     case 15: action_position[7] = joint_states_position[7] - self._joint_increment_value;
        #             break;
        #     case 16: action_position[8] = joint_states_position[8] + self._joint_increment_value;
        #             break;
        #     case 17: action_position[8] = joint_states_position[8] - self._joint_increment_value;
        #             break;
        #     case 18: action_position[9] = joint_states_position[9] + self._joint_increment_value;
        #             break;
        #     case 19: action_position[9] = joint_states_position[9] - self._joint_increment_value;
        #             break;
        #     case 20: action_position[10] = joint_states_position[10] + self._joint_increment_value;
        #             break;
        #     case 21: action_position[10] = joint_states_position[10] - self._joint_increment_value;
        #             break;
        #     case 22: action_position[11] = joint_states_position[11] + self._joint_increment_value;
        #             break;
        #     case 23: action_position[11] = joint_states_position[11] - self._joint_increment_value;
        #             break;
        #     case 24: action_position[12] = joint_states_position[12] + self._joint_increment_value;
        #             break;
        #     case 25: action_position[12] = joint_states_position[12] - self._joint_increment_value;
        #             break;
        #     case 26: action_position[13] = joint_states_position[13] + self._joint_increment_value;
        #             break;
        #     case 27: action_position[13] = joint_states_position[13] - self._joint_increment_value;
        #             break;
        #     case 28: action_position[14] = joint_states_position[14] + self._joint_increment_value;
        #             break;
        #     case 29: action_position[14] = joint_states_position[14] - self._joint_increment_value;
        #             break;
        #     case 30: action_position[15] = joint_states_position[15] + self._joint_increment_value;
        #             break;
        #     case 31: action_position[15] = joint_states_position[15] - self._joint_increment_value;
        #             break;
        #     case 32: action_position[16] = joint_states_position[16] + self._joint_increment_value;
        #             break;
        #     case 33: action_position[16] = joint_states_position[16] - self._joint_increment_value;
        #             break;
        #     case 34: action_position[17] = joint_states_position[17] + self._joint_increment_value;
        #             break;
        #     case 35: action_position[17] = joint_states_position[17] - self._joint_increment_value;
        #             break;
        #     case 36: action_position[18] = joint_states_position[18] + self._joint_increment_value;
        #             break;
        #     case 37: action_position[18] = joint_states_position[18] - self._joint_increment_value;
        #             break;
        #     case 38: action_position[19] = joint_states_position[19] + self._joint_increment_value;
        #             break;
        #     case 39: action_position[19] = joint_states_position[19] - self._joint_increment_value;
        #             break;
        #     case 40: action_position[20] = joint_states_position[20] + self._joint_increment_value;
        #             break;
        #     case 41: action_position[20] = joint_states_position[20] - self._joint_increment_value;
        #             break;
        #     case 42: action_position[21] = joint_states_position[21] + self._joint_increment_value;
        #             break;
        #     case 43: action_position[21] = joint_states_position[21] - self._joint_increment_value;
        #             break;
        #     case 44: action_position[22] = joint_states_position[22] + self._joint_increment_value;
        #             break;
        #     case 45: action_position[22] = joint_states_position[22] - self._joint_increment_value;
        #             break;
        #     default: action_position = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0];
        #              break;
        # }