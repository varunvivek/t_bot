#!/usr/bin/env python

import rospy
import math
from std_msgs.msg import String
from std_msgs.msg import Float64
import time
class JointPub(object):
    def __init__(self):

        self.publishers_array = []
        self._joint_pub=[]
        # joint_names_array=['hip_joint','lb_bf','lb_ff','lb_l','lb_m','lb_u','lb_x',
        #                                 'lf_bf','lf_ff','lf_l','lf_u','lf_x',
        #                                 'rb_bf','rb_ff','rb_l','rb_m','rb_u','rb_x',
        #                                 'rf_bf','rf_ff','rf_l','rf_u','rf_x']


        # self._hip_joint_pub = rospy.Publisher('/t_bot_new/hip_joint_controller/command', Float64, queue_size=1)
        self._lb_bf_joint_pub = rospy.Publisher('/t_bot_new/lb_bf_joint_controller/command', Float64, queue_size=1)
        self._lb_ff_joint_pub = rospy.Publisher('/t_bot_new/lb_ff_joint_controller/command', Float64, queue_size=1)
        self._lb_l_joint_pub = rospy.Publisher('/t_bot_new/lb_l_joint_controller/command', Float64, queue_size=1)
        self._lb_m_joint_pub = rospy.Publisher('/t_bot_new/lb_m_joint_controller/command', Float64, queue_size=1)
        self._lb_u_joint_pub = rospy.Publisher('/t_bot_new/lb_u_joint_controller/command', Float64, queue_size=1)
        # self._lb_x_joint_pub = rospy.Publisher('/t_bot_new/lb_x_joint_controller/command', Float64, queue_size=1)
        self._lf_bf_joint_pub = rospy.Publisher('/t_bot_new/lf_bf_joint_controller/command', Float64, queue_size=1)
        self._lf_ff_joint_pub = rospy.Publisher('/t_bot_new/lf_ff_joint_controller/command', Float64, queue_size=1)
        self._lf_l_joint_pub = rospy.Publisher('/t_bot_new/lf_l_joint_controller/command', Float64, queue_size=1)
        self._lf_u_joint_pub = rospy.Publisher('/t_bot_new/lf_u_joint_controller/command', Float64, queue_size=1)
        # self._lf_x_joint_pub = rospy.Publisher('/t_bot_new/lf_x_joint_controller/command', Float64, queue_size=1)
        self._rb_bf_joint_pub = rospy.Publisher('/t_bot_new/rb_bf_joint_controller/command', Float64, queue_size=1)
        self._rb_ff_joint_pub = rospy.Publisher('/t_bot_new/rb_ff_joint_controller/command', Float64, queue_size=1)
        self._rb_l_joint_pub = rospy.Publisher('/t_bot_new/rb_l_joint_controller/command', Float64, queue_size=1)
        self._rb_m_joint_pub = rospy.Publisher('/t_bot_new/rb_m_joint_controller/command', Float64, queue_size=1)
        self._rb_u_joint_pub = rospy.Publisher('/t_bot_new/rb_u_joint_controller/command', Float64, queue_size=1)
        # self._rb_x_joint_pub = rospy.Publisher('/t_bot_new/rb_x_joint_controller/command', Float64, queue_size=1)
        self._rf_bf_joint_pub = rospy.Publisher('/t_bot_new/rf_bf_joint_controller/command', Float64, queue_size=1)
        self._rf_ff_joint_pub = rospy.Publisher('/t_bot_new/rf_ff_joint_controller/command', Float64, queue_size=1)
        self._rf_l_joint_pub = rospy.Publisher('/t_bot_new/rf_l_joint_controller/command', Float64, queue_size=1)
        self._rf_u_joint_pub = rospy.Publisher('/t_bot_new/rf_u_joint_controller/command', Float64, queue_size=1)
        # self._rf_x_joint_pub = rospy.Publisher('/t_bot_new/rf_x_joint_controller/command', Float64, queue_size=1)

        
        # self.publishers_array.append(self._hip_joint_pub)
        self.publishers_array.append(self._lb_bf_joint_pub)
        self.publishers_array.append(self._lb_ff_joint_pub)
        self.publishers_array.append(self._lb_l_joint_pub)
        self.publishers_array.append(self._lb_m_joint_pub)
        self.publishers_array.append(self._lb_u_joint_pub)
        # self.publishers_array.append(self._lb_x_joint_pub)
        self.publishers_array.append(self._lf_bf_joint_pub)
        self.publishers_array.append(self._lf_ff_joint_pub)
        self.publishers_array.append(self._lf_l_joint_pub)
        self.publishers_array.append(self._lf_u_joint_pub)
        # self.publishers_array.append(self._lf_x_joint_pub)
        self.publishers_array.append(self._rb_bf_joint_pub)
        self.publishers_array.append(self._rb_ff_joint_pub)
        self.publishers_array.append(self._rb_l_joint_pub)
        self.publishers_array.append(self._rb_m_joint_pub)
        self.publishers_array.append(self._rb_u_joint_pub)
        # self.publishers_array.append(self._rb_x_joint_pub)
        self.publishers_array.append(self._rf_bf_joint_pub)
        self.publishers_array.append(self._rf_ff_joint_pub)
        self.publishers_array.append(self._rf_l_joint_pub)
        self.publishers_array.append(self._rf_u_joint_pub)
        # self.publishers_array.append(self._rf_x_joint_pub)
        self.init_pos = [0]*len(self.publishers_array)

    def set_init_pose(self):
        """
        Sets joints to initial position [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        :return:
        """
        self.check_publishers_connection()
        self.move_joints(self.init_pos, self.init_pos)


    def check_publishers_connection(self):
        """
        Checks that all the publishers are working
        :return:
        """
        rate = rospy.Rate(10)  # 10hz
        # while (self._hip_joint_pub.get_num_connections() == 0):
        #     rospy.logdebug("No susbribers to _hip_joint_pub yet so we wait and try again")
        #     try:
        #         rate.sleep()
        #     except rospy.ROSInterruptException:
        #         # This is to avoid error when world is rested, time when backwards.
        #         pass
        # rospy.logdebug("_hip_joint_pub Publisher Connected")

        while (self._lb_bf_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _lb_bf_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_lb_bf_joint_pub Publisher Connected")

        while (self._lb_ff_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _lb_ff_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_lb_ff_joint_pub Publisher Connected")

        while (self._lb_l_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _lb_l_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_lb_l_joint_pub Publisher Connected")

        while (self._lb_m_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _lb_m_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_lb_m_joint_pub Publisher Connected")

        while (self._lb_u_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _lb_u_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_lb_u_joint_pub Publisher Connected")

        # while (self._lb_x_joint_pub.get_num_connections() == 0):
        #     rospy.logdebug("No susbribers to _lb_x_joint_pub yet so we wait and try again")
        #     try:
        #         rate.sleep()
        #     except rospy.ROSInterruptException:
        #         # This is to avoid error when world is rested, time when backwards.
        #         pass
        # rospy.logdebug("_lb_x_joint_pub Publisher Connected")

        while (self._lf_bf_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _lf_bf_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_lf_bf_joint_pub Publisher Connected")

        while (self._lf_ff_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _lf_ff_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_lf_ff_joint_pub Publisher Connected")

        while (self._lf_l_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _lf_l_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_lf_l_joint_pub Publisher Connected")

        while (self._lf_u_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _lf_u_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_lf_u_joint_pub Publisher Connected")

        # while (self._lf_x_joint_pub.get_num_connections() == 0):
        #     rospy.logdebug("No susbribers to _lf_x_joint_pub yet so we wait and try again")
        #     try:
        #         rate.sleep()
        #     except rospy.ROSInterruptException:
        #         # This is to avoid error when world is rested, time when backwards.
        #         pass
        # rospy.logdebug("_lf_x_joint_pub Publisher Connected")

        while (self._rb_bf_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _rb_bf_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_rb_bf_joint_pub Publisher Connected")

        while (self._rb_ff_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _rb_ff_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_rb_ff_joint_pub Publisher Connected")

        while (self._rb_l_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _rb_l_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_rb_l_joint_pub Publisher Connected")

        while (self._rb_m_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _rb_m_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_rb_m_joint_pub Publisher Connected")

        while (self._rb_u_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _rb_u_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_rb_u_joint_pub Publisher Connected")

        # while (self._rb_x_joint_pub.get_num_connections() == 0):
        #     rospy.logdebug("No susbribers to _rb_x_joint_pub yet so we wait and try again")
        #     try:
        #         rate.sleep()
        #     except rospy.ROSInterruptException:
        #         # This is to avoid error when world is rested, time when backwards.
        #         pass
        # rospy.logdebug("_rb_x_joint_pub Publisher Connected")

        while (self._rf_bf_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _rf_bf_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_rf_bf_joint_pub Publisher Connected")

        while (self._rf_ff_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _rf_ff_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_rf_ff_joint_pub Publisher Connected")

        while (self._rf_l_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _rf_l_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_rf_l_joint_pub Publisher Connected")

        while (self._rf_u_joint_pub.get_num_connections() == 0):
            rospy.logdebug("No susbribers to _rf_u_joint_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_rf_u_joint_pub Publisher Connected")

        # while (self._rf_x_joint_pub.get_num_connections() == 0):
        #     rospy.logdebug("No susbribers to _rf_x_joint_pub yet so we wait and try again")
        #     try:
        #         rate.sleep()
        #     except rospy.ROSInterruptException:
        #         # This is to avoid error when world is rested, time when backwards.
        #         pass
        # rospy.logdebug("_rf_x_joint_pub Publisher Connected")

        rospy.logdebug("All Publishers READY")

    def joint_mono_des_callback(self, msg):
        rospy.logdebug(str(msg.joint_state.position))

        self.move_joints(msg.joint_state.position)

    def move_joints_2(self, joints_array, joint_states_position):
        # print("joints_array: "+ str(joints_array))
        if(len(joint_states_position)>18):
            del_list = [0,6,11,17,22]
            for d in del_list:
                del joint_states_position[d]
        
        i = 0
        # print('joints_array: '+str(joints_array))
        for publisher_object in self.publishers_array:
            joint_value = Float64()
            joint_state = joint_states_position[i]
            joints_array_val = joints_array[i]
            step = 0.05
            if joint_state < joints_array_val:
                while joint_state <= joints_array_val:
                    joint_state  += step
                    joint_value.data = joint_state
                    # rospy.logdebug("JointsPos>>"+str(joint_value))
                    print(joint_value.data)
                    publisher_object.publish(joint_value)
            elif joint_state > joints_array_val:
                while joint_state >= joints_array_val:
                    joint_state  -= step
                    joint_value.data = joint_state
                    # rospy.logdebug("JointsPos>>"+str(joint_value))
                    print(joint_value.data)
                    publisher_object.publish(joint_value)
            else:
                joint_value.data = joints_array_val
            i += 1

    def move_joints_1(self, joints_array):

        i = 0
        for publisher_object in self.publishers_array:
          joint_value = Float64()
          joint_value.data = joints_array[i]
          rospy.logdebug("JointsPos>>"+str(joint_value))
          publisher_object.publish(joint_value)
          i += 1

    def sign_(self, num):
        if(num==0):
            return 1
        else:
            return (num/abs(num))

    def move_joints_sin(self, joints_array, joint_states_position):
        #print("joint_states_position: "+ str(len(joint_states_position)))
        # if(len(joint_states_position)>18):
        #     # del_list = [0,6,11,17,22]
        #     del_list = [0,5,9,14,18]
        #     for d in del_list:
        #         del joint_states_position[d]
        
        i = 0
        # if(len(joints_array)<18):
        #     print('joints_array: '+str(joints_array) )
        for publisher_object in self.publishers_array:
            joint_value = Float64()
            final = joint_states_position[i]
            initial = joints_array[i]
            w = 0.00
            if(final==0.0):
                final=0.1
            #print(initial)
            #print(final)
            #print("============")
            if initial < final:
                #print("++++++++++++++")
                while initial <= final:                  
                    initial  += (final) * abs(math.sin(w)) * (self.sign_(final))
                    w+=0.05
                    joint_value.data = initial
                    publisher_object.publish(joint_value)
            elif initial > final:
                #print("----------")
                while initial >= final:                  
                    initial  -= (final) * abs(math.sin(w)) * (self.sign_(final))
                    w+=0.05
                    joint_value.data = initial
                    publisher_object.publish(joint_value)
            else:
                #print("/////////")
                joint_value.data = initial
                publisher_object.publish(joint_value)
            i += 1


    def move_joints_slow(self, joints_array, joint_states_position):
        
        publisher_object = self.publishers_array
        joint_value = Float64()
        final = joint_states_position
        initial = joints_array
        step=[]

        for k in range(0,len(initial)):
            step.append(abs((final[k]-initial[k])/1000))

        i = 0
        while i<1000:
            for j in [17,13,12,16,11,15,10,17,9]:
                if initial[j]<final[j]:
                    initial[j] += step[j]
                elif initial[j]>final[j]:
                    initial[j] -= step[j]
                joint_value.data = initial[j]
                publisher_object[j].publish(joint_value)

            i+=1
        time.sleep(0.07)
        i = 0
        while i<1000:
            for j in [8,4,3,7,2,6,1,5,0]:
                if initial[j]<final[j]:
                    initial[j] += step[j]
                elif initial[j]>final[j]:
                    initial[j] -= step[j]
                joint_value.data = initial[j]
                publisher_object[j].publish(joint_value)
            i+=1
    
    def move_joints(self, joints_array, joint_states_position):
        
        publisher_object = self.publishers_array
        joint_value = Float64()
        final = joint_states_position
        initial = joints_array
        step=[]
        step_size=200
        for k in range(0,len(initial)):
            step.append(abs((final[k]-initial[k])/step_size))

        i = 0
        while i<step_size:
            for j in [0,1,2,3,4,14,15,16,17]:
                if initial[j]<=final[j]:
                    initial[j] += step[j]
                elif initial[j]=>final[j]:
                    initial[j] -= step[j]
                joint_value.data = initial[j]
                publisher_object[j].publish(joint_value)

            i+=1
        time.sleep(0.05)
        i = 0
        while i<step_size:
            for j in [5,6,7,8,9,10,11,12,13]:
                if initial[j]<=final[j]:
                    initial[j] += step[j]
                elif initial[j]=>final[j]:
                    initial[j] -= step[j]
                joint_value.data = initial[j]
                publisher_object[j].publish(joint_value)
            i+=1




    def start_loop(self, rate_value = 2.0):
        rospy.logdebug("Start Loop")
        pos1 = [0.0,0.0,0.0,0.0,1.6,0.0,0.0,0,0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        pos2 = [0.0,0.0,0.0,0.0,-1.6,0.0,0.0,0,0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        position = "pos1"
        print(len(pos1))
        self.move_joints(self.init_pos,self.init_pos)
        rate = rospy.Rate(rate_value)
        while not rospy.is_shutdown():
          if position == "pos1":
            self.move_joints(pos1,self.init_pos)
            
            position = "pos2"
          else:
            self.move_joints(pos2,pos1)
            position = "pos1"
          rate.sleep()

    def start_sinus_loop(self, rate_value = 2.0):
        rospy.logdebug("Start Loop")
        w = 0.0
        x = 2.0*math.sin(w)
        #pos_x = [0.0,0.0,x]
        #pos_x = [x, 0.0, 0.0]
        pos_x = [0.0,0.0,0.0,0.0,x,0.0,0.0,0,0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0,0,0.0]
        rate = rospy.Rate(rate_value)
        while not rospy.is_shutdown():
            self.move_joints_1(pos_x)
            w += 0.05
            x = 2.0 * math.sin(w)
            print(x)
            #pos_x = [0.0, 0.0, x]
            #pos_x = [x, 0.0, 0.0]
            pos_x = [0.0,0.0,0.0,0.0,x,0.0,0.0,0,0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0,0,0.0]
            rate.sleep()


if __name__=="__main__":
    rospy.init_node('joint_publisher_node')
    joint_publisher = JointPub()
    rate_value = 50.0
    # joint_publisher.start_loop(rate_value)
    joint_publisher.start_sinus_loop(rate_value)
