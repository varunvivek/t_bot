#!/usr/bin/env python

import os
import time
import random
import numpy as np
import matplotlib.pyplot as plt
# import pybullet_envs
import gym
import torch
import torch.nn as nn
import torch.nn.functional as F
from gym import wrappers
from torch.autograd import Variable
from collections import deque

# import numpy
import random
# import qlearn #custom py file
from std_msgs.msg import Float64
# ROS packages required
import rospy
import rospkg

# import our training environment
import tbot_env #custom py file

class Actor(nn.Module):
  
  def __init__(self, state_dim, action_dim, max_action):
    super(Actor, self).__init__()
    self.layer_1 = nn.Linear(state_dim, 400)
    self.layer_2 = nn.Linear(400, 300)
    self.layer_3 = nn.Linear(300, action_dim)
    self.max_action = max_action

  def forward(self, x):
    x = F.relu(self.layer_1(x))
    x = F.relu(self.layer_2(x))
    x = self.max_action * torch.tanh(self.layer_3(x))
    return x

class Critic(nn.Module):
  
  def __init__(self, state_dim, action_dim):
    super(Critic, self).__init__()
    # Defining the first Critic neural network
    self.layer_1 = nn.Linear(state_dim + action_dim, 400)
    self.layer_2 = nn.Linear(400, 300)
    self.layer_3 = nn.Linear(300, 1)
    # Defining the second Critic neural network
    self.layer_4 = nn.Linear(state_dim + action_dim, 400)
    self.layer_5 = nn.Linear(400, 300)
    self.layer_6 = nn.Linear(300, 1)

  def forward(self, x, u):
    xu = torch.cat([x, u], 1)
    # Forward-Propagation on the first Critic Neural Network
    x1 = F.relu(self.layer_1(xu))
    x1 = F.relu(self.layer_2(x1))
    x1 = self.layer_3(x1)
    # Forward-Propagation on the second Critic Neural Network
    x2 = F.relu(self.layer_4(xu))
    x2 = F.relu(self.layer_5(x2))
    x2 = self.layer_6(x2)
    return x1, x2

  def Q1(self, x, u):
    xu = torch.cat([x, u], 1)
    x1 = F.relu(self.layer_1(xu))
    x1 = F.relu(self.layer_2(x1))
    x1 = self.layer_3(x1)
    return x1

# Selecting the device (CPU or GPU)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(device)
# Building the whole Training Process into a class


class TD3(object):
  
  def __init__(self, state_dim, action_dim, max_action):
    self.actor = Actor(state_dim, action_dim, max_action).to(device)
    self.actor_target = Actor(state_dim, action_dim, max_action).to(device)
    self.actor_target.load_state_dict(self.actor.state_dict())
    self.actor_optimizer = torch.optim.Adam(self.actor.parameters())
    self.critic = Critic(state_dim, action_dim).to(device)
    self.critic_target = Critic(state_dim, action_dim).to(device)
    self.critic_target.load_state_dict(self.critic.state_dict())
    self.critic_optimizer = torch.optim.Adam(self.critic.parameters())
    self.max_action = max_action

  def select_action(self, state):
    # #print("state")
    # #print(state)
    state = torch.Tensor(state.reshape(1, -1)).to(device)
    action = self.actor(state).cpu().data.numpy().flatten()
    # #print("action")
    # #print(action)
    return self.actor(state).cpu().data.numpy().flatten()

  def train(self, replay_buffer, iterations, batch_size=100, discount=0.99, taunoise_clip=0.005, policy_noise=0.2, noise_clip=0.5, policy_freq=2):
    # print("train++++++++++++++++++")
    for it in range(iterations):

      batch_states, batch_next_states, batch_actions, batch_rewards, batch_dones = replay_buffer.sample(batch_size)
      state = torch.Tensor(batch_states).to(device)
      next_state = torch.Tensor(batch_next_states).to(device)
      # print('batch_actions: ' + str(batch_actions) + '\nshape :'+str(batch_actions.shape))
      if type(batch_actions)==np.ndarray:
        batch_action=[]
        for a in batch_actions:
          aa=a.tolist()
          if len(aa)<41:
            aa=aa[0]
          batch_action.append(aa)
          # print('batch_action_+++++++++++: ')
          # print('end for loop')
        batch_actions = batch_action

        # print('np to list convert: '+str(batch_actions.shape())+str(type(batch_actions)))
        # print('batch_actions new: ' + str((batch_actions)))
      # print('batch_actions type: ' + str(type(batch_actions)))
      # print('iterations: '+str(it))
      # if type(batch_actions)
      action = torch.Tensor(batch_actions).to(device)
      reward = torch.Tensor(batch_rewards).to(device)
      done = torch.Tensor(batch_dones).to(device)

      next_action = self.actor_target(next_state)
      #print("next_action: " + str(next_action))
      # #print("batch_actions_size: " + str(batch_actions.size()))
      #print("batch_actions: " + str(batch_actions))
      noise = torch.Tensor(batch_actions).data.normal_(0, policy_noise).to(device)
      #print("noise_befpre_clip: " + str((batch_actions)))
      noise = noise.clamp(-noise_clip, noise_clip)
      #print("noise: " + str(noise.size()))
      next_action = (next_action + noise).clamp(-self.max_action, self.max_action)
      
      target_Q1, target_Q2 = self.critic_target(next_state, next_action)
      
      target_Q = torch.min(target_Q1, target_Q2)
      
      target_Q = reward + ((1 - done) * discount * target_Q).detach()
      
      current_Q1, current_Q2 = self.critic(state, action)
      
      critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(current_Q2, target_Q)

      self.critic_optimizer.zero_grad()
      critic_loss.backward()
      self.critic_optimizer.step()
      
      if it % policy_freq == 0:
        actor_loss = -self.critic.Q1(state, self.actor(state)).mean()
        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()
        
        for param, target_param in zip(self.actor.parameters(), self.actor_target.parameters()):
          target_param.data.copy_(tau * param.data + (1 - tau) * target_param.data)
        
        for param, target_param in zip(self.critic.parameters(), self.critic_target.parameters()):
          target_param.data.copy_(tau * param.data + (1 - tau) * target_param.data)
  
  # Making a save method to save a trained model
  def save(self, filename, directory):
    torch.save(self.actor.state_dict(), '%s/%s_actor.pth' % (directory, filename))
    torch.save(self.critic.state_dict(), '%s/%s_critic.pth' % (directory, filename))
  
  # Making a load method to load a pre-trained model
  def load(self, filename, directory):
    self.actor.load_state_dict(torch.load('%s/%s_actor.pth' % (directory, filename)))
    self.critic.load_state_dict(torch.load('%s/%s_critic.pth' % (directory, filename)))


def evaluate_policy(policy, eval_episodes=10):
  avg_reward = 0.
  for _ in range(eval_episodes):
    #print("before env reset")
    obs = env.reset()
    done = False
    while not done:
      #print("before select_action")
      #print(type(obs))
      action = policy.select_action(np.array(obs))
      
      #print("before env.step(action)")
      obs, reward, done, _ = env.step(action)
      avg_reward += reward
  avg_reward /= eval_episodes
  print ("---------------------------------------")
  print ("Average Reward over the Evaluation Step: %f" % (avg_reward))
  print ("---------------------------------------")
  return avg_reward

def mkdir(base, name):
  path = os.path.join(base, name)
  if not os.path.exists(path):
      os.makedirs(path)
  return path
work_dir = mkdir('exp', 'brs')
monitor_dir = mkdir(work_dir, 'monitor')

if __name__ == '__main__':
    
    rospy.init_node('tbot_gym', anonymous=True, log_level=rospy.INFO)

    # Create the Gym environment
    env_name = 'tbot-v0'
    env = gym.make(env_name)
    rospy.logdebug ( "Gym environment done")
    reward_pub = rospy.Publisher('/t_bot/reward', Float64, queue_size=1)
    episode_reward_pub = rospy.Publisher('/t_bot/episode_reward', Float64, queue_size=1)

    # Set the logging system
    rospack = rospkg.RosPack()
    pkg_path = rospack.get_path('t_bot_training')
    outdir = pkg_path + '/training_results'
    env = wrappers.Monitor(env, outdir, force=True)
    rospy.logdebug("Monitor Wrapper started")
    
    last_time_steps = np.ndarray(0)

    seed = 0 # Random seed number
    #parameters removed

    file_name = "%s_%s_%s" % ("TD3", env_name, str(seed))
    print ("---------------------------------------")
    print ("Settings: %s" % (file_name))
    print ("---------------------------------------")


    eval_episodes = 10
    save_env_vid = True
    env = gym.make(env_name)
    max_episode_steps = env._max_episode_steps
    if save_env_vid:
        env = wrappers.Monitor(env, monitor_dir, force = True)
        env.reset()
    env.seed(seed)
    torch.manual_seed(seed)
    np.random.seed(seed)
    state_dim = 28
    action_dim = 18
    max_action = 1.0
    policy = TD3(state_dim, action_dim, max_action)
    policy.load(file_name, './pytorch_models/')
    _ = evaluate_policy(policy, eval_episodes=eval_episodes)